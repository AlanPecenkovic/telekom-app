import { useNavigation } from "@react-navigation/native";
import { SearchBar } from "@rneui/base";
import React, { useLayoutEffect, useState } from "react";
import { TouchableWithoutFeedback } from "react-native";
import { FlatList, StyleSheet, View } from "react-native";
import { USERS } from "../../data/dummy-data";
import Bell_User_icons from "../ui/Bell_User_icons";
import ListEquipment from "../ui/ListEquipment";
import NotificationsDropdown from "../ui/NotificationsDropdown";
import UserIconDropdown from "../ui/UserIconDropdown";

const userdata = USERS;

const ToolLendingScreen = () => {
  const navigation = useNavigation();
  const [search, setSearch] = useState("");
  const [userDropdownVisible, setUserDropdownVisible] = useState(false);
  const [bellDropdownVisible, setBellDropdownVisible] = useState(false);
  //Bell and user icons dropdown functions start
  function handleUserDropdown() {
    setUserDropdownVisible(!userDropdownVisible);
    setBellDropdownVisible(false);
  }
  function hideUserDropdown() {
    setUserDropdownVisible(false);
  }
  function handleBellDropdown() {
    navigation.navigate("Profile", {
      showNotifications: true,
    });
    // setBellDropdownVisible(!bellDropdownVisible);
    setUserDropdownVisible(false);
  }
  function hideBellDropdown() {
    setBellDropdownVisible(false);
  }

  function handleLogout() {
    navigation.navigate("Start");
  }
  function handleProfile() {
    navigation.navigate("Profile");
  }
  //Bell and user icons dropdown functions end

  useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => {
        return (
          <>
            <Bell_User_icons
              onUserPress={handleUserDropdown}
              onBellPress={handleBellDropdown}
            />
            {userDropdownVisible && (
              <UserIconDropdown
                onLogout={handleLogout}
                onProfile={handleProfile}
                style={styles.userIconDropdown}
              />
            )}
            {/* {bellDropdownVisible && (
              <NotificationsDropdown style={styles.bellIconDropdown} />
            )} */}
          </>
        );
      },
    });
    const resetDropdowns = navigation.addListener("state", (e) => {
      setBellDropdownVisible(false);
      setUserDropdownVisible(false);
    });
    return resetDropdowns;
  }, [navigation, handleLogout, handleProfile]);

  return (
    <TouchableWithoutFeedback
      onPress={() => {
        hideBellDropdown(), hideUserDropdown();
      }}
    >
      <View style={styles.container}>
        <FlatList
          ListHeaderComponent={
            <SearchBar
              placeholder="Pretraži..."
              onChangeText={setSearch}
              value={search}
              containerStyle={{
                backgroundColor: "#fff",
                alignItems: "center",
                justifyContent: "center",
                marginBottom: 14,
                marginTop: 16,
                width: 350,
                height: 76,
                borderColor: "#D8D6DE",
                borderBottomColor: "white",
                borderTopColor: "white",
                borderRadius: 10,
                elevation: 8,
              }}
              inputContainerStyle={{
                width: 310,
                height: 36,
                backgroundColor: "#fff",
                alignSelf: "center",
                borderRadius: 5,
                elevation: 2,
              }}
            />
          }
          ListHeaderComponentStyle={{
            alignItems: "center",
          }}
          ListFooterComponent={<ListEquipment userData={userdata} title="Oprema" />}
        />
      </View>
    </TouchableWithoutFeedback>
  );
};

export default ToolLendingScreen;

const styles = StyleSheet.create({
  userIconDropdown: { left: 112 },
  bellIconDropdown: { left: -90 },
  container: {
    backgroundColor: "#fff",
  },
});
