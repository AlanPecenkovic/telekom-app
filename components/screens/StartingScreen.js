import { Button } from "@rneui/base";
import React from "react";
import { StyleSheet, Text, View } from "react-native";

const StartingScreen = ({ navigation }) => {
  function goToLogin() {
    navigation.navigate("Login");
  }

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Telekom app</Text>
      <Button
        title="Dalje"
        onPress={goToLogin}
        buttonStyle={{
          width: 350,
          height: 45,
          borderWidth: 1,
          borderColor: "#D8D6DE",
          backgroundColor: "#fff",
        }}
        titleStyle={{
          color: "#7D788E",
          fontFamily: "montserrat500",
        }}
        color="#D8D6DE"
        radius={5}
        type="outline"
      />
    </View>
  );
};

export default StartingScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#013A89",
  },
  title: {
    color: "#eee",
    fontFamily: "gugi",
    fontSize: 36,
    fontWeight: "500",
    marginVertical: 50,
  },
});
