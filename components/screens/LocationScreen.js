import React, { useLayoutEffect, useState } from "react";
import { useNavigation } from "@react-navigation/native";
import { Image, StyleSheet, Text, View } from "react-native";
import Bell_User_icons from "../ui/Bell_User_icons";
import UserIconDropdown from "../ui/UserIconDropdown";
import NotificationsDropdown from "../ui/NotificationsDropdown";
import { TouchableWithoutFeedback } from "react-native";
import MapView, { Marker } from "react-native-maps";

const LocationScreen = ({ route }) => {
  const navigation = useNavigation();
  //   const { user } = route.params;
  const [userDropdownVisible, setUserDropdownVisible] = useState(false);
  const [bellDropdownVisible, setBellDropdownVisible] = useState(false);
  const [selectedLocation, setSelectedLocation] = useState();

  //Bell and user icons dropdown functions start
  function handleUserDropdown() {
    setUserDropdownVisible(!userDropdownVisible);
    setBellDropdownVisible(false);
  }
  function hideUserDropdown() {
    setUserDropdownVisible(false);
  }
  function handleBellDropdown() {
    setBellDropdownVisible(!bellDropdownVisible);
    setUserDropdownVisible(false);
  }
  function hideBellDropdown() {
    setBellDropdownVisible(false);
  }

  function handleLogout() {
    navigation.navigate("Start");
  }
  function handleProfile() {
    navigation.navigate("Profile");
  }
  //Bell and user icons dropdown functions end

  const region = {
    latitude: 44.823789969470894,
    longitude: 15.876034237764783,
    latitudeDelta: 0.0922,
    longitudeDelta: 0.0421,
  };

  function selectLocation(event) {
    const lat = event.nativeEvent.coordinate.latitude;
    const lng = event.nativeEvent.coordinate.longitude;

    setSelectedLocation({ lat: lat, lng: lng });
  }

  useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => {
        return (
          <>
            <Bell_User_icons
              onUserPress={handleUserDropdown}
              onBellPress={handleBellDropdown}
            />

            {userDropdownVisible && (
              <UserIconDropdown
                onLogout={handleLogout}
                onProfile={handleProfile}
                style={styles.userIconDropdown}
              />
            )}

            {bellDropdownVisible && (
              <NotificationsDropdown style={styles.bellIconDropdown} />
            )}
          </>
        );
      },
    });
    const resetDropdowns = navigation.addListener("state", (e) => {
      setBellDropdownVisible(false);
      setUserDropdownVisible(false);
    });
    return resetDropdowns;
  }, [navigation, handleLogout]);

  return (
    <TouchableWithoutFeedback
      onPress={() => {
        hideBellDropdown(), hideUserDropdown();
      }}
    >
      <View style={styles.container}>
        <Text style={styles.text}>Lokacija korisnika:</Text>
        <View style={styles.innerContainer}>
          <MapView style={styles.map} initialRegion={region} onPress={selectLocation}>
            {selectedLocation && (
              <Marker
                title="Odabrana lokacija"
                coordinate={{
                  latitude: selectedLocation.lat,
                  longitude: selectedLocation.lng,
                }}
              />
            )}
          </MapView>
          {/* <Image
            style={styles.image}
            source={require("../../assets/images/Location.webp")}
          /> */}
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
};

export default LocationScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "flex-end",
    alignItems: "center",
    backgroundColor: "#fff",
  },
  innerContainer: {
    width: 390,
    height: 290,
    marginTop: 29,
    marginBottom: 10,
    backgroundColor: "#fff",
    borderWidth: 1,
    borderRadius: 10,
    borderColor: "#D8D6DE",
    overflow: "hidden",
  },
  map: {
    width: "100%",
    height: "100%",
  },
  text: {
    fontFamily: "montserrat400",
    fontSize: 16,
    color: "#7D788E",
  },
  image: {
    width: "100%",
    height: "100%",
  },
});
