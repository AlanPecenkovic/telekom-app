import { useNavigation } from "@react-navigation/native";
import { Button } from "@rneui/base";
import { React, useState, useLayoutEffect, useEffect } from "react";
import { StyleSheet, Text, TouchableWithoutFeedback, View } from "react-native";
import Bell_User_icons from "../ui/Bell_User_icons";
import MapIcon from "../ui/MapIcon";
import NotificationsDropdown from "../ui/NotificationsDropdown";
import UserIconDropdown from "../ui/UserIconDropdown";
import WorkEntryModal from "../ui/WorkEntryModal";
import EmployeeAdminDetails from "../ui/EmployeeAdminDetails";

const EmployeeAdminDetailsScreen = ({ route }) => {
  const navigation = useNavigation();
  const { user } = route.params;
  const [userDropdownVisible, setUserDropdownVisible] = useState(false);
  const [bellDropdownVisible, setBellDropdownVisible] = useState(false);
  const [pauseIconPressed, setPauseIconPressed] = useState(false);
  const [xIconPressed, setXIconPressed] = useState(false);
  const [started, setStarted] = useState(false);
  const [time, setTime] = useState(0);
  const [modalVisible, setModalVisible] = useState(false);

  //Bell and user icons dropdown functions start
  function handleUserDropdown() {
    setUserDropdownVisible(!userDropdownVisible);
    setBellDropdownVisible(false);
  }
  function hideUserDropdown() {
    setUserDropdownVisible(false);
  }
  function handleBellDropdown() {
    setBellDropdownVisible(!bellDropdownVisible);
    setUserDropdownVisible(false);
  }
  function hideBellDropdown() {
    setBellDropdownVisible(false);
  }

  function handleLogout() {
    navigation.navigate("Start");
  }
  function handleProfile() {
    navigation.navigate("Profile");
  }
  //Bell and user icons dropdown functions end

  useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => {
        return (
          <>
            <Bell_User_icons
              onUserPress={handleUserDropdown}
              onBellPress={handleBellDropdown}
            />

            {userDropdownVisible && (
              <UserIconDropdown
                onLogout={handleLogout}
                onProfile={handleProfile}
                style={styles.userIconDropdown}
              />
            )}

            {bellDropdownVisible && (
              <NotificationsDropdown style={styles.bellIconDropdown} />
            )}
          </>
        );
      },
    });
    const resetDropdowns = navigation.addListener("state", (e) => {
      setBellDropdownVisible(false);
      setUserDropdownVisible(false);
    });
    return resetDropdowns;
  }, [navigation, handleLogout]);

  function handleStartButton() {
    setStarted(true);
    // setXIconPressed(!xIconPressed);
  }

  function handleSubmitButton() {
    //Time submitted, save time and open modal
    setModalVisible(true);
    setUserDropdownVisible(false);
    // console.log("Time passed in seconds: ", time);
    setStarted(false);
  }

  function handlePauseButton() {
    setPauseIconPressed(!pauseIconPressed);
    setStarted(!started);
  }

  function handleLocation() {
    navigation.navigate("Location");
  }

  // function handleXButton() {
  //   setXIconPressed(!xIconPressed);
  //   setStarted(false);
  //   setTime(0);
  //   console.log("X button logic...");
  // }

  //Timing function START
  useEffect(() => {
    let interval = null;
    if (started) {
      interval = setInterval(() => {
        setTime((prevTime) => prevTime + 1);
      }, 1000);
    } else {
      clearInterval(interval);
    }
    return () => {
      clearInterval(interval);
    };
  }, [started]);
  //Timing function END

  function dismissModal() {
    setModalVisible(false);
  }

  return (
    <TouchableWithoutFeedback
      onPress={() => {
        hideBellDropdown(), hideUserDropdown();
      }}
    >
      <View style={styles.container}>
        <EmployeeAdminDetails
          id={user.clientId}
          name={user.clientName}
          address={user.address}
          clientCity={user.clientCity}
        />

        <View style={styles.buttonsContainer}>
          <Button
            title={
              !started
                ? "Start"
                : `${("0" + Math.floor((time / 3600) % 24)).slice(-2)}: ${(
                    "0" + Math.floor((time / 60) % 60)
                  ).slice(-2)} : ${("0" + (time % 60)).slice(-2)}`
            }
            buttonStyle={{
              width: 175,
              height: 45,
              marginRight: 70,
              borderWidth: started ? 1 : 0,
              borderColor: "#013A89",
            }}
            titleStyle={{
              fontFamily: "montserrat600",
              fontSize: 16,
              color: started ? "#6A657E" : "white",
            }}
            color={started ? "white" : "#013A89"}
            radius={5}
            onPress={handleStartButton}
          />
          {/* <Button
            icon={xIconPressed ? styles.xIconActive : styles.xIconInactive}
            color={xIconPressed ? "#32CC77" : "#013A89"}
            buttonStyle={{
              width: 45,
              height: 45,
              marginRight: 15,
            }}
            radius={5}
            onPress={handleXButton}
          /> */}
          <Button
            icon={styles.xIconActive}
            color={"#32CC77"}
            buttonStyle={{
              width: 45,
              height: 45,
              marginRight: 15,
            }}
            radius={5}
            onPress={handleSubmitButton}
          />
          <Button
            icon={pauseIconPressed ? styles.pauseIconActive : styles.pauseIconInactive}
            color="#fff"
            raised
            containerStyle={{
              height: 45,
              width: 45,
              justifyContent: "center",
              alignItems: "center",
            }}
            buttonStyle={{ justifyContent: "center", alignItems: "center" }}
            radius={5}
            onPress={handlePauseButton}
          />
        </View>
        <Button
          title="Pogledaj lokaciju"
          color="#fff"
          raised
          buttonStyle={{
            width: 350,
            alignSelf: "center",
            marginVertical: 4,
          }}
          titleStyle={{
            fontFamily: "montserrat600",
            fontSize: 16,
            color: "#6A657E",
            marginRight: 10,
          }}
          icon={<MapIcon />}
          iconRight
          radius={5}
          onPress={handleLocation}
        />
        <WorkEntryModal modalVisible={modalVisible} dismissModal={dismissModal} />
      </View>
    </TouchableWithoutFeedback>
  );
};

export default EmployeeAdminDetailsScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
  },
  buttonsContainer: {
    flexDirection: "row",
    width: 350,
    marginBottom: 25,
  },
  userIconDropdown: {
    left: 115,
  },
  bellIconDropdown: {
    left: -86,
  },
  xIconActive: {
    name: "check",
    type: "feather",
    color: "#fff",
    size: 17,
    marginLeft: -1,
  },
  xIconInactive: {
    name: "x",
    type: "feather",
    color: "#fff",
    size: 22,
    marginLeft: -3,
  },
  pauseIconActive: {
    name: "pause",
    type: "fontisto",
    color: "#013A89",
    size: 17,
  },
  pauseIconInactive: {
    name: "control-pause",
    type: "simple-line-icon",
    color: "#7D788E",
    size: 17,
    marginLeft: -3,
  },
});
