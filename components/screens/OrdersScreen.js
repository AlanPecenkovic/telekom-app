import React, { useLayoutEffect, useState } from "react";
import { useNavigation } from "@react-navigation/native";
import Bell_User_icons from "../ui/Bell_User_icons";
import { FlatList, StyleSheet, Text, View } from "react-native";
import { Icon, SearchBar } from "@rneui/base";
import ListComponent from "../ui/ListComponent";
import { USERS } from "../../data/dummy-data";
import SelectList from "react-native-dropdown-select-list";
import OrderDetailsModal from "../ui/OrderDetailsModal";
import UserIconDropdown from "../ui/UserIconDropdown";
import NotificationsDropdown from "../ui/NotificationsDropdown";
import { TouchableWithoutFeedback } from "react-native";
import ShowDropdown from "../ui/ShowDropdown";
import NumbersDropdown from "../ui/NumbersDropdown";

const userdata = USERS;

const OrdersScreen = () => {
  const navigation = useNavigation();
  const [search, setSearch] = useState("");
  const [modalVisible, setModalVisible] = useState(false);
  const [touchedUser, setTouchedUser] = useState({
    id: "1",
    name: "Amar Imsirovic",
    username: "Amar",
    email: "amar@gmail.com",
    address: {
      street: "Turija street",
      suite: "Apt. 556",
      city: "Bihac",
      zipcode: "77000",
    },
    phone: "1-770-736-8031 x56442",
  });
  const [userDropdownVisible, setUserDropdownVisible] = useState(false);
  const [bellDropdownVisible, setBellDropdownVisible] = useState(false);
  const [numbersDropdown, setNumbersDropdown] = useState(false);
  const [selected, setSelected] = useState("");
  const data = [
    { key: "1", value: "Sve" },
    { key: "2", value: "Najnoviji" },
    { key: "3", value: "Najstariji" },
  ];
  //Bell and user icons dropdown functions start
  function handleUserDropdown() {
    setUserDropdownVisible(!userDropdownVisible);
    setBellDropdownVisible(false);
  }
  function hideUserDropdown() {
    setUserDropdownVisible(false);
  }
  function handleBellDropdown() {
    navigation.navigate("Profile", {
      showNotifications: true,
    });
    // setBellDropdownVisible(!bellDropdownVisible);
    setUserDropdownVisible(false);
  }
  function hideBellDropdown() {
    setBellDropdownVisible(false);
  }

  function handleLogout() {
    navigation.navigate("Start");
  }
  function handleProfile() {
    navigation.navigate("Profile");
  }
  //Bell and user icons dropdown functions end

  function handleOrderDetails(data) {
    setUserDropdownVisible(false);
    setBellDropdownVisible(false);
    setModalVisible(true);
    setTouchedUser(data);
  }

  function dismissModal() {
    setModalVisible(!modalVisible);
  }

  function handleShowDropdown() {
    setNumbersDropdown(!numbersDropdown);
  }

  useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => {
        return (
          <>
            <Bell_User_icons
              onUserPress={handleUserDropdown}
              onBellPress={handleBellDropdown}
            />
            {userDropdownVisible && (
              <UserIconDropdown
                onLogout={handleLogout}
                onProfile={handleProfile}
                style={styles.userIconDropdown}
              />
            )}
            {/* {bellDropdownVisible && (
              <NotificationsDropdown style={styles.bellIconDropdown} />
            )} */}
          </>
        );
      },
    });
    const resetDropdowns = navigation.addListener("state", (e) => {
      setBellDropdownVisible(false);
      setUserDropdownVisible(false);
    });
    return resetDropdowns;
  }, [navigation, handleLogout, handleProfile]);

  return (
    <TouchableWithoutFeedback
      onPress={() => {
        hideBellDropdown(), hideUserDropdown();
      }}
    >
      <View style={styles.container}>
        <OrderDetailsModal
          modalVisible={modalVisible}
          dismissModal={dismissModal}
          touchedUser={touchedUser}
        />

        <FlatList
          ListHeaderComponent={
            <View style={styles.headerContainer}>
              <View style={styles.inputsContainer}>
                <Text style={styles.title}>Filtriraj</Text>
                <SelectList
                  setSelected={setSelected}
                  data={data}
                  onSelect={() => console.log(data)}
                  boxStyles={{
                    width: 310,
                    height: 38,
                    marginTop: 10,
                    marginBottom: 22,
                    borderWidth: 1,
                    borderColor: "#D8D6DE",
                    borderRadius: 5,
                    // backgroundColor: "#ccc",
                  }}
                  inputStyles={{
                    fontFamily: "montserrat400",
                    fontSize: 15,
                    color: "#7D788E",
                    marginTop: -6,
                    // backgroundColor: "red",
                  }}
                  dropdownStyles={{
                    borderWidth: 1,
                    borderColor: "#D8D6DE",
                    borderRadius: 10,
                    width: 310,
                    marginBottom: 12,
                  }}
                  dropdownTextStyles={{
                    fontFamily: "montserrat400",
                    fontSize: 16,
                    color: "#7D788E",
                    marginBottom: 16,
                    paddingTop: 4,
                    paddingLeft: 3,
                  }}
                  arrowicon={
                    <Icon
                      name="chevron-thin-down"
                      type="entypo"
                      size={14}
                      color={"grey"}
                    />
                  }
                  placeholder="Odaberi status"
                  search={false}
                />

                <SearchBar
                  placeholder="Pretraži..."
                  onChangeText={setSearch}
                  value={search}
                  containerStyle={{
                    backgroundColor: "#fff",
                    marginTop: -10,
                    marginBottom: 10,
                    borderBottomColor: "white",
                    borderTopColor: "white",
                  }}
                  inputContainerStyle={{
                    width: 310,
                    height: 36,
                    backgroundColor: "#fff",
                    alignSelf: "center",
                    borderRadius: 5,
                    borderWidth: 1,
                    borderColor: "#D8D6DE",
                  }}
                  inputStyle={{
                    fontFamily: "montserrat400",
                    fontSize: 16,
                    color: "#7D788E",
                    marginTop: -4,
                    paddingLeft: 10,
                  }}
                  searchIcon={false}
                />
                {/* Bottom line fix for searchbox input */}
                <View
                  style={{
                    width: 307,
                    borderTopWidth: 1,
                    borderTopColor: "#D8D6DE",
                    marginTop: -20,
                    borderTopLeftRadius: 5,
                  }}
                ></View>
                {/* Bottom line fix for searchbox input */}
                <ShowDropdown
                  title="Prikaz"
                  containerStyle={{
                    width: 346,
                    height: 50,
                    marginTop: 10,
                    marginBottom: 10,
                    padding: 0,
                    elevation: 0,
                  }}
                  onPress={handleShowDropdown}
                />
              </View>
              {numbersDropdown && (
                <NumbersDropdown containerStyle={{ marginLeft: -119 }} />
              )}
            </View>
          }
          ListFooterComponent={
            <ListComponent
              userData={userdata}
              title="Svi nalozi"
              onPress={handleOrderDetails}
            />
          }
        />
      </View>
    </TouchableWithoutFeedback>
  );
};

export default OrdersScreen;

const styles = StyleSheet.create({
  userIconDropdown: { left: 142 },
  bellIconDropdown: { left: -58 },
  container: {
    backgroundColor: "#fff",
  },
  headerContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 20,
  },
  inputsContainer: {
    alignItems: "center",
    width: 350,
    borderWidth: 1,
    borderColor: "#D8D6DE",
    borderRadius: 10,
    padding: 5,
    marginTop: 12,
  },
  title: {
    fontFamily: "montserrat400",
    fontSize: 16,
    alignSelf: "flex-start",
    marginTop: 20,
    marginLeft: 14,
    marginBottom: 10,
    color: "#7D788E",
  },
});
