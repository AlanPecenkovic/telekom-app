import { Icon } from "@rneui/base";
import React, { useLayoutEffect } from "react";
import LoginForm from "../ui/LoginForm";

const LoginScreen = ({ navigation }) => {
  function handleBackNav() {
    navigation.navigate("Start");
  }

  useLayoutEffect(() => {
    navigation.setOptions({
      headerLeft: () => {
        return (
          <Icon
            raised
            name="left"
            type="antdesign"
            size={22}
            color="#7D788E"
            containerStyle={{ elevation: 6 }}
            onPress={handleBackNav}
          />
        );
      },
    });
  }, [navigation, handleBackNav]);

  return <LoginForm />;
};

export default LoginScreen;
