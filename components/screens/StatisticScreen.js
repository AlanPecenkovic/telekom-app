import React, { useEffect, useLayoutEffect, useState } from "react";
import { useNavigation, useRoute } from "@react-navigation/native";
import { ScrollView, StyleSheet, Text, View } from "react-native";
import Bell_User_icons from "../ui/Bell_User_icons";
import CircularProgress from "react-native-circular-progress-indicator";
import * as Progress from "react-native-progress";
import DateTimePicker from "@react-native-community/datetimepicker";
import DateDropdown from "../ui/DateDropdown";
import NotificationsDropdown from "../ui/NotificationsDropdown";
import UserIconDropdown from "../ui/UserIconDropdown";
import { TouchableWithoutFeedback } from "react-native";

const StatisticScreen = () => {
  const navigation = useNavigation();
  const [value, setValue] = useState(0);
  const [date, setDate] = useState(new Date());
  const [mode, setMode] = useState("date");
  const [show, setShow] = useState(false);
  const [userDropdownVisible, setUserDropdownVisible] = useState(false);
  const [bellDropdownVisible, setBellDropdownVisible] = useState(false);
  //Bell and user icons dropdown functions start
  function handleUserDropdown() {
    setUserDropdownVisible(!userDropdownVisible);
    setBellDropdownVisible(false);
  }
  function hideUserDropdown() {
    setUserDropdownVisible(false);
  }
  function handleBellDropdown() {
    navigation.navigate("Profile", {
      showNotifications: true,
    });
    // setBellDropdownVisible(!bellDropdownVisible);
    setUserDropdownVisible(false);
  }
  function hideBellDropdown() {
    setBellDropdownVisible(false);
  }

  function handleLogout() {
    navigation.navigate("Start");
  }
  function handleProfile() {
    navigation.navigate("Profile");
  }

  // const route = useRoute();
  // useEffect(() => {
  //   setUserDropdownVisible(false);
  // }, [route]);

  //Bell and user icons dropdown functions end

  // datepicker functions start
  const onChange = (event, selectedDate) => {
    const currentDate = selectedDate;
    setShow(false);
    setDate(currentDate);
  };

  const showMode = (currentMode) => {
    if (Platform.OS === "android") {
      setShow(true);
      // for iOS, add a button that closes the picker
    }
    setMode(currentMode);
  };

  const showDatepicker = () => {
    showMode("date");
  };
  // datepicker functions end

  const handleStatistic = () => {
    navigation.navigate("EmployeeAdmin");
  };

  useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => {
        return (
          <>
            <Bell_User_icons
              onUserPress={handleUserDropdown}
              onBellPress={handleBellDropdown}
            />
            {userDropdownVisible && (
              <UserIconDropdown
                onLogout={handleLogout}
                onProfile={handleProfile}
                style={styles.userIconDropdown}
              />
            )}
            {/* {bellDropdownVisible && (
              <NotificationsDropdown style={styles.bellIconDropdown} />
            )} */}
          </>
        );
      },
    });

    const resetDropdowns = navigation.addListener("state", (e) => {
      setBellDropdownVisible(false);
      setUserDropdownVisible(false);
    });
    return resetDropdowns;
  }, [navigation, handleStatistic]);

  return (
    <ScrollView>
      <TouchableWithoutFeedback
        onPress={() => {
          hideBellDropdown(), hideUserDropdown();
        }}
      >
        <View style={styles.container}>
          <DateDropdown onPress={showDatepicker} title="Odaberi datum" />

          <View>
            {show && (
              <DateTimePicker
                testID="dateTimePicker"
                value={date}
                mode={mode}
                datePickerStyle={"calendar"}
                is24Hour={true}
                onChange={onChange}
              />
            )}
          </View>
          <Text style={styles.text}>Nalozi</Text>
          <View style={styles.chartContainer}>
            <Progress.Circle
              progress={0.7}
              size={110}
              color="#2ECA76"
              animated={true}
              // indeterminate={true}
              borderWidth={1}
              showsText={true}
              formatText={() => {
                return "100%";
              }}
              textStyle={{
                fontFamily: "montserrat400",
                fontSize: 20,
                color: "#7D788E",
              }}
            />
          </View>
          <Text style={styles.text}>Potencijalni korisnici</Text>
          <View style={styles.chartContainer}>
            <CircularProgress
              radius={60}
              value={85}
              valueSuffix={"%"}
              progressValueFontSize={20}
              progressValueColor={"#7D788E"}
              progressValueStyle={{
                fontFamily: "montserrat400",
                fontWeight: "400",
              }}
              duration={1500}
              // title={"završeno"}
              // titleColor={"grey"}
              // titleStyle={{ fontWeight: "bold" }}
              inActiveStrokeColor={"#2ecc71"}
              inActiveStrokeOpacity={0.2}
              inActiveStrokeWidth={4}
              activeStrokeWidth={4}
              onAnimationComplete={() => {
                // When animation completes do something
              }}
            />
          </View>
        </View>
      </TouchableWithoutFeedback>
    </ScrollView>
  );
};

export default StatisticScreen;

const styles = StyleSheet.create({
  userIconDropdown: { left: 120 },
  bellIconDropdown: { left: -80 },
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#fff",
  },
  chartContainer: {
    justifyContent: "center",
    alignItems: "center",
    width: 350,
    height: 250,
    marginVertical: 10,
    padding: 10,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: "#ccc",
  },
  text: {
    fontFamily: "montserrat400",
    color: "#6A657E",
    fontSize: 16,
    marginLeft: 30,
    alignSelf: "flex-start",
  },
  datePickerStyle: {
    width: 200,
    marginTop: 20,
  },
});
