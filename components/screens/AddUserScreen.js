import { useNavigation } from "@react-navigation/native";
import { Button } from "@rneui/base";
import React, { useContext, useLayoutEffect, useState } from "react";
import { FlatList, StyleSheet, Text, View } from "react-native";
import { USERS } from "../../data/dummy-data";
import Bell_User_icons from "../ui/Bell_User_icons";
import AddUserModal from "../ui/AddUserModal";
import NotificationsDropdown from "../ui/NotificationsDropdown";
import UserIconDropdown from "../ui/UserIconDropdown";
import { TouchableWithoutFeedback } from "react-native";
import ShowDropdown from "../ui/ShowDropdown";
import NumbersDropdown from "../ui/NumbersDropdown";
import AddUserList from "../ui/AddUserList";
import { UserListContext } from "../../store/userListContext";
import axios from "axios";
import { apiEndpoint } from "../../data/apiEndpoint";

const userdata = USERS;

const AddUser = () => {
  const navigation = useNavigation();
  const usersCtx = useContext(UserListContext);
  const [modalVisible, setModalVisible] = useState(false);
  const [numbersDropdown, setNumbersDropdown] = useState(false);
  const [users, setUsers] = useState([]);
  const [touchedUser, setTouchedUser] = useState({
    id: "1",
    name: "Amar Imsirovic",
    username: "Amar",
    email: "amar@gmail.com",
    address: {
      street: "Turija street",
      suite: "Apt. 556",
      city: "Bihac",
      zipcode: "77000",
    },
    phone: "1-770-736-8031 x56442",
  });
  console.log(usersCtx.token, usersCtx.user_id);

  const [userDropdownVisible, setUserDropdownVisible] = useState(false);
  const [bellDropdownVisible, setBellDropdownVisible] = useState(false);
  //Bell and user icons dropdown functions start
  function handleUserDropdown() {
    setUserDropdownVisible(!userDropdownVisible);
    setBellDropdownVisible(false);
  }
  function hideUserDropdown() {
    setUserDropdownVisible(false);
  }
  function handleBellDropdown() {
    navigation.navigate("Profile", {
      showNotifications: true,
    });
    // setBellDropdownVisible(!bellDropdownVisible);
    setUserDropdownVisible(false);
  }
  function hideBellDropdown() {
    setBellDropdownVisible(false);
  }

  function handleLogout() {
    navigation.navigate("Start");
  }
  function handleProfile() {
    navigation.navigate("Profile");
  }
  //Bell and user icons dropdown functions end

  function handleUserDetails(data) {
    setTouchedUser(data);
    setUserDropdownVisible(false);
    setBellDropdownVisible(false);
    navigation.navigate("AddUserDetailsScreen", { user: data });
  }

  function dismissModal() {
    setModalVisible(!modalVisible);
  }

  const handleAddUserModal = (userObject) => {
    setModalVisible(!modalVisible);
    setUsers((prevUsers) => [...prevUsers, userObject]);
    // console.log(userObject);

    const headersConfig = {
      Authorization: `Bearer ${usersCtx.token}`,
      "Content-Type": "application/json",
    };
    axios({
      method: "post",
      url: apiEndpoint + "api/clients/add-new",
      data: {
        ...userObject,
      },
      headers: headersConfig,
    })
      .then((response) => {
        console.log(response.data);
      })
      .catch((error) => console.log(error));
  };

  function handleShowDropdown() {
    setNumbersDropdown(!numbersDropdown);
  }

  useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => {
        return (
          <>
            <Bell_User_icons
              onUserPress={handleUserDropdown}
              onBellPress={handleBellDropdown}
            />
            {userDropdownVisible && (
              <UserIconDropdown
                onLogout={handleLogout}
                onProfile={handleProfile}
                style={styles.userIconDropdown}
              />
            )}
            {/* {bellDropdownVisible && (
              <NotificationsDropdown style={styles.bellIconDropdown} />
            )} */}
          </>
        );
      },
    });
    const resetDropdowns = navigation.addListener("state", (e) => {
      setBellDropdownVisible(false);
      setUserDropdownVisible(false);
    });
    return resetDropdowns;
  }, [navigation, handleLogout, handleProfile]);

  return (
    <TouchableWithoutFeedback
      onPress={() => {
        hideBellDropdown(), hideUserDropdown();
      }}
    >
      <View style={styles.container}>
        <View style={styles.centeredView}>
          <AddUserModal
            modalVisible={modalVisible}
            handleAddUserModal={handleAddUserModal}
            dismissModal={dismissModal}
          />
        </View>
        {/* <FlatList
        data={users}
        renderItem={(user) => <Text>{user.item.name}</Text>}
      /> */}
        <FlatList
          ListHeaderComponent={
            <>
              <Button
                title="Dodaj  +"
                buttonStyle={{
                  width: 350,
                  height: 45,
                  alignSelf: "center",
                  marginTop: 50,
                }}
                titleStyle={{
                  fontFamily: "montserrat600",
                  fontSize: 16,
                }}
                color="#013A89"
                radius={5}
                onPress={dismissModal}
              />
              <ShowDropdown title="Prikaz" onPress={handleShowDropdown} />
              {numbersDropdown && <NumbersDropdown />}
            </>
          }
          ListFooterComponent={
            <AddUserList userData={users} onPress={handleUserDetails} />
          }
        />
      </View>
    </TouchableWithoutFeedback>
  );
};

export default AddUser;

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#fff",
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#fff",
  },
  userIconDropdown: {
    left: 41,
  },
  bellIconDropdown: {
    left: -160,
  },
});
