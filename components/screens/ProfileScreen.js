import { React, useEffect, useState } from "react";
import { useNavigation } from "@react-navigation/native";
import {
  StyleSheet,
  View,
  TouchableWithoutFeedback,
  Keyboard,
  Text,
  ScrollView,
  FlatList,
} from "react-native";
import { useLayoutEffect } from "react";
import { Button, Icon } from "@rneui/base";
import Bell_User_icons from "../ui/Bell_User_icons";
import UserIconDropdown from "../ui/UserIconDropdown";
import NotificationsDropdown from "../ui/NotificationsDropdown";
import NewPasswordForm from "../ui/NewPasswordForm";
import NotificationsList from "../ui/NotificationsList";
import ProfileOverview from "../ui/ProfileOverview";

const ProfileScreen = ({ route }) => {
  const navigation = useNavigation();
  const [userDropdownVisible, setUserDropdownVisible] = useState(false);
  const [bellDropdownVisible, setBellDropdownVisible] = useState(false);
  const [userButtonActive, setUserButtonActive] = useState(true);
  const [lockButtonActive, setLockButtonActive] = useState(false);
  const [bellButtonActive, setBellButtonActive] = useState(false);
  //Bell and user icons dropdown functions start
  function handleUserDropdown() {
    setUserDropdownVisible(!userDropdownVisible);
    setBellDropdownVisible(false);
  }
  function hideUserDropdown() {
    setUserDropdownVisible(false);
  }
  function handleBellDropdown() {
    setBellDropdownVisible(!bellDropdownVisible);
    setUserDropdownVisible(false);
  }
  function hideBellDropdown() {
    setBellDropdownVisible(false);
  }

  function handleLogout() {
    navigation.navigate("Start");
  }
  function handleProfile() {
    // When profile is pressed refetch data or do nothing
  }
  //Bell and user icons dropdown functions end

  function handleBackNav() {
    navigation.goBack();
  }

  function handleOverviewButton() {
    setUserButtonActive(!userButtonActive);
    setLockButtonActive(false);
    setBellButtonActive(false);
  }
  function handleLockButton() {
    setLockButtonActive(!lockButtonActive);
    setUserButtonActive(false);
    setBellButtonActive(false);
  }
  function handleBellButton() {
    setBellButtonActive(!bellButtonActive);
    setUserButtonActive(false);
    setLockButtonActive(false);
  }

  useLayoutEffect(() => {
    navigation.setOptions({
      headerTitle: "Profil",
      headerTitleStyle: {
        fontFamily: "montserrat500",
        fontSize: 14,
        color: "#6A657E",
      },
      headerLeft: () => {
        return (
          <Icon
            raised
            name="left"
            type="antdesign"
            size={22}
            color="#7D788E"
            containerStyle={{ elevation: 6 }}
            onPress={handleBackNav}
          />
        );
      },
      headerRight: () => {
        return (
          <>
            <Bell_User_icons
              onUserPress={handleUserDropdown}
              onBellPress={handleBellDropdown}
            />
            {userDropdownVisible && (
              <UserIconDropdown
                onLogout={handleLogout}
                onProfile={handleProfile}
                style={styles.userIconDropdown}
              />
            )}
            {bellDropdownVisible && (
              <NotificationsDropdown style={styles.bellIconDropdown} />
            )}
          </>
        );
      },
    });
    const resetDropdowns = navigation.addListener("state", (e) => {
      setBellDropdownVisible(false);
      setUserDropdownVisible(false);
    });
    return resetDropdowns;
  }, [navigation, handleLogout, handleProfile]);

  useEffect(() => {
    if (route.params && route.params.showNotifications) {
      setBellButtonActive(true);
      setUserButtonActive(false);
      setLockButtonActive(false);
    }
    return () => {
      setUserButtonActive(true);
      setLockButtonActive(false);
      setBellButtonActive(false);
    };
  }, [route.params]);

  return (
    <TouchableWithoutFeedback
      onPress={() => {
        hideBellDropdown(), hideUserDropdown();
        Keyboard.dismiss();
      }}
    >
      <View style={styles.outerContainer}>
        <FlatList
          // ListHeaderComponent={<View></View>}
          ListFooterComponent={
            <View style={styles.buttonsContainer}>
              <Button
                title="Općenito"
                // type="clear"
                color={userButtonActive ? "#013A89" : "white"}
                titleStyle={
                  userButtonActive
                    ? styles.buttonActiveTitleStyle
                    : styles.buttonTitleStyle
                }
                buttonStyle={styles.buttonStyle}
                icon={{
                  name: "user",
                  type: "feather",
                  size: 16,
                  color: userButtonActive ? "white" : "#6E6B7B",
                  marginRight: -1,
                }}
                onPress={handleOverviewButton}
              />
              <Button
                title="Promijeni lozinku"
                color={lockButtonActive ? "#013A89" : "white"}
                titleStyle={
                  lockButtonActive
                    ? styles.buttonActiveTitleStyle
                    : styles.buttonTitleStyle
                }
                buttonStyle={styles.buttonStyle}
                icon={{
                  name: "lock",
                  type: "octicon",
                  size: 16,
                  color: lockButtonActive ? "white" : "#6E6B7B",
                  paddingLeft: 2,
                }}
                onPress={handleLockButton}
              />
              <Button
                title="Obavijesti"
                color={bellButtonActive ? "#013A89" : "white"}
                titleStyle={
                  bellButtonActive
                    ? styles.buttonActiveTitleStyle
                    : styles.buttonTitleStyle
                }
                buttonStyle={styles.buttonStyle}
                icon={{
                  name: "bell",
                  type: "simple-line-icon",
                  size: 16,
                  color: bellButtonActive ? "white" : "#6E6B7B",
                }}
                onPress={handleBellButton}
              />
              <View>
                {userButtonActive ? <ProfileOverview /> : null}
                {lockButtonActive ? <NewPasswordForm /> : null}
                {bellButtonActive ? <NotificationsList /> : null}
              </View>
            </View>
          }
        />
      </View>
    </TouchableWithoutFeedback>
  );
};

export default ProfileScreen;

const styles = StyleSheet.create({
  outerContainer: {
    flex: 1,
    width: "100%",
    backgroundColor: "white",
  },
  buttonsContainer: {
    flex: 1,
    height: "100%",
    alignItems: "center",
    backgroundColor: "#fff",
    paddingTop: 25,
    paddingBottom: 30,
  },
  text: {
    fontFamily: "montserrat400",
    fontSize: 14,
    color: "#6A657E",
  },
  userIconDropdown: {
    left: 90,
  },
  bellIconDropdown: {
    left: -110,
  },
  buttonActiveTitleStyle: {
    fontFamily: "montserrat400",
    fontSize: 14,
    paddingLeft: 10,
  },
  buttonTitleStyle: {
    fontFamily: "montserrat400",
    fontSize: 14,
    paddingLeft: 10,
    color: "#6A657E",
  },
  buttonStyle: {
    width: 350,
    height: 45,
    borderRadius: 5,
    justifyContent: "flex-start",
    paddingLeft: 26,
  },
});
