import { Icon } from "@rneui/base";
import React, { useLayoutEffect } from "react";
import ForgetPassForm from "../ui/ForgetPassForm";

const ForgetPasswordScreen = ({ navigation }) => {
  function handleBackNav() {
    navigation.navigate("Login");
  }

  useLayoutEffect(() => {
    navigation.setOptions({
      headerLeft: () => {
        return (
          <Icon
            raised
            name="left"
            type="antdesign"
            size={22}
            color="#7D788E"
            containerStyle={{ elevation: 6 }}
            onPress={handleBackNav}
          />
        );
      },
    });
  }, [navigation, handleBackNav]);

  return <ForgetPassForm />;
};

export default ForgetPasswordScreen;
