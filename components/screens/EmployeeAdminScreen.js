import React, { useContext, useEffect, useLayoutEffect, useState } from "react";
import {
  FlatList,
  StyleSheet,
  TouchableWithoutFeedback,
  Text,
  View,
} from "react-native";
import axios from "axios";
import { apiEndpoint } from "../../data/apiEndpoint";
import { UserListContext } from "../../store/userListContext";
import Bell_User_icons from "../ui/Bell_User_icons";
import NotificationsDropdown from "../ui/NotificationsDropdown";
import UserIconDropdown from "../ui/UserIconDropdown";
import { useIsFocused } from "@react-navigation/native";
import EmployeeAdminList from "../ui/EmployeeAdminList";

const EmployeeAdminScreen = ({ navigation, route }) => {
  const { token, userData } = route.params;

  const usersCtx = useContext(UserListContext);
  const [dailyTasks, setDailyTasks] = useState("Nema zadataka");
  const isFocused = useIsFocused();

  const [userDropdownVisible, setUserDropdownVisible] = useState(false);
  const [bellDropdownVisible, setBellDropdownVisible] = useState(false);
  //Bell and user icons dropdown functions start
  function handleUserDropdown() {
    setUserDropdownVisible(!userDropdownVisible);
    setBellDropdownVisible(false);
  }
  function hideUserDropdown() {
    setUserDropdownVisible(false);
  }
  function handleBellDropdown() {
    navigation.navigate("Profile", {
      showNotifications: true,
    });
    // setBellDropdownVisible(!bellDropdownVisible);
    setUserDropdownVisible(false);
  }
  function hideBellDropdown() {
    setBellDropdownVisible(false);
  }

  function handleLogout() {
    navigation.navigate("Start");
  }

  function handleProfile() {
    navigation.navigate("Profile");
  }
  //Bell and user icons dropdown functions end
  function handleDetails(selectedUser) {
    // const headersConfig = {
    //   "Authorization": `Bearer ${usersCtx.token}`,
    //   "Content-Type": "application/json",
    // };
    // axios({
    //   method: "post",
    //   url: apiEndpoint + "api/daily-tasks/details",
    //   data: { id: userData.user.id },
    //   headers: headersConfig,
    // })
    //   .then((response) => console.log(response.data.response))
    //   .catch((response) => console.log(response));

    navigation.navigate("EmployeeAdminDetails", { user: selectedUser });
  }

  useEffect(() => {
    const headersConfig = {
      Authorization: `Bearer ${usersCtx.token}`,
      "Content-Type": "application/json",
    };
    axios({
      method: "post",
      url: apiEndpoint + "api/daily-tasks",
      data: { user_id: userData.user.id },
      headers: headersConfig,
    })
      .then((response) => {
        setDailyTasks(response.data.response);
        // console.log(response.data.response);
        // console.log(dailyTasks);
      })
      .catch((error) => console.log(error));
  }, [isFocused]);

  let list = "";

  if (dailyTasks !== "There are no tasks for today.") {
    list = (
      <FlatList
        ListHeaderComponent={<View style={{ marginBottom: 50 }}></View>} // Empty view margin fix
        ListFooterComponent={
          <EmployeeAdminList
            userData={dailyTasks}
            title="Dnevni zadaci"
            onPress={handleDetails}
          />
        }
      />
    );
  } else {
    list = <Text style={styles.noTasks}>Nema dnevnih zadataka</Text>;
  }

  useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => {
        return (
          <>
            <Bell_User_icons
              onUserPress={handleUserDropdown}
              onBellPress={handleBellDropdown}
            />

            {userDropdownVisible && (
              <UserIconDropdown onLogout={handleLogout} onProfile={handleProfile} />
            )}

            {/* {bellDropdownVisible && <NotificationsDropdown />} */}
          </>
        );
      },
    });
    const resetDropdowns = navigation.addListener("state", (e) => {
      setBellDropdownVisible(false);
      setUserDropdownVisible(false);
    });
    return resetDropdowns;
  }, [navigation, handleLogout]);

  return (
    <TouchableWithoutFeedback
      onPress={() => {
        hideBellDropdown(), hideUserDropdown();
      }}
    >
      <View style={styles.container}>
        {/* <FlatList
          ListHeaderComponent={<View style={{ marginBottom: 50 }}></View>} // Empty view margin fix
          ListFooterComponent={
            <ListComponent
              // userData={usersCtx.usersList}
              userData={dailyTasks}
              title="Dnevni zadaci"
              onPress={handleDetails}
            />
          }
        /> */}
        {list}
      </View>
    </TouchableWithoutFeedback>
  );
};

export default EmployeeAdminScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  noTasks: {
    textAlign: "center",
    marginTop: 60,
    fontSize: 20,
    fontFamily: "montserrat400",
    color: "#6A657E",
  },
});
