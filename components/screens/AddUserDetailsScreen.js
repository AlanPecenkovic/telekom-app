import { React, useState, useContext } from "react";
import { useNavigation } from "@react-navigation/native";
import {
  StyleSheet,
  View,
  Text,
  Keyboard,
  TextInput,
  TouchableWithoutFeedback,
} from "react-native";
import { Button, CheckBox, Icon } from "@rneui/base";
import { useLayoutEffect } from "react";
import Bell_User_icons from "../ui/Bell_User_icons";
import UserIconDropdown from "../ui/UserIconDropdown";
import NotificationsDropdown from "../ui/NotificationsDropdown";
import { UserListContext } from "../../store/userListContext";
import { Formik } from "formik";
import * as yup from "yup";
import { ScrollView } from "react-native";

const AddUserDetailsScreen = ({ route }) => {
  const navigation = useNavigation();
  const selectedUser = useContext(UserListContext);
  const [users, setUsers] = useState([
    {
      name: "Alan",
      address: "Ulica bb",
      telephone: "061999999",
      email: "alan@alan.com",
      date: "2022-08-16",
    },
  ]);

  const [toggleCheckBox, setToggleCheckBox] = useState(false);
  const [userDropdownVisible, setUserDropdownVisible] = useState(false);
  const [bellDropdownVisible, setBellDropdownVisible] = useState(false);
  //Bell and user icons dropdown functions start
  function handleUserDropdown() {
    setUserDropdownVisible(!userDropdownVisible);
    setBellDropdownVisible(false);
  }
  function hideUserDropdown() {
    setUserDropdownVisible(false);
  }
  function handleBellDropdown() {
    setBellDropdownVisible(!bellDropdownVisible);
    setUserDropdownVisible(false);
  }
  function hideBellDropdown() {
    setBellDropdownVisible(false);
  }

  function handleLogout() {
    navigation.navigate("Start");
  }
  function handleProfile() {
    navigation.navigate("Profile");
  }
  //Bell and user icons dropdown functions end

  function handleBackNav() {
    navigation.goBack();
  }

  useLayoutEffect(() => {
    navigation.setOptions({
      headerStyle: {
        width: 200,
        height: 200,
      },
      headerLeft: () => {
        return (
          <Icon
            raised
            name="left"
            type="antdesign"
            size={22}
            color="#7D788E"
            containerStyle={{
              elevation: 6,
              overflow: "hidden",
            }}
            onPress={handleBackNav}
          />
        );
      },
      headerRight: () => {
        return (
          <>
            <Bell_User_icons
              onUserPress={handleUserDropdown}
              onBellPress={handleBellDropdown}
            />
            {/* {userDropdownVisible && (
              <UserIconDropdown
                onLogout={handleLogout}
                onProfile={handleProfile}
                style={styles.userIconDropdown}
              />
            )}
            {bellDropdownVisible && (
              <NotificationsDropdown style={styles.bellIconDropdown} />
            )} */}
            {/* Dropdowns are hidden in header below and can not be displayed here */}
          </>
        );
      },
    });
  }, [navigation, handleLogout, handleProfile]);

  const addUserSchema = yup.object({
    name: yup.string().required("Ime i prezime je obavezno unijeti").min(3),
    address: yup.string().required("Unesite adresu stanovanja").min(4),
    telephone: yup.number().integer().required("Unesite broj telefona"),
    email: yup.string().email("Molim Vas unesite ispravnu email adresu"),
    // password: yup.string(),  //Needed for the Login screen
    date: yup.date().max(new Date()),
  });

  const handleAddUser = (userObject) => {
    setUsers((currentUsers) => {
      return [userObject, ...currentUsers];
    });
  };
  return (
    <TouchableWithoutFeedback
      onPress={() => {
        hideBellDropdown(), hideUserDropdown();
        Keyboard.dismiss();
      }}
    >
      <View style={styles.container}>
        {/* Dropdowns are displayed in the component but offset position near the header buttons */}
        {userDropdownVisible && (
          <UserIconDropdown
            onLogout={handleLogout}
            onProfile={handleProfile}
            style={styles.userIconDropdown}
          />
        )}
        {bellDropdownVisible && (
          <NotificationsDropdown style={styles.bellIconDropdown} />
        )}
        {/* Dropdowns are displayed in the component but offset position near the header buttons */}
        <ScrollView>
          <View style={styles.userContainer}>
            <Icon
              reverse
              name="user"
              type="feather"
              size={50}
              color="#F5F5F5"
              iconStyle={{ color: "#6A657E" }}
            />
            <View style={styles.titleContainer}>
              <Text style={styles.title}>{route.params.user.name}</Text>
              <Button
                title="Spremi promjene"
                titleStyle={{ fontFamily: "montserrat600" }}
                color="#013A89"
                radius={5}
                buttonStyle={{ width: 175, height: 45 }}
                onPress={handleBackNav}
              />
            </View>
          </View>

          <Formik
            initialValues={{
              name: "",
              address: "",
              telephone: "",
              email: "",
              date: "",
            }}
            validationSchema={addUserSchema}
            onSubmit={(values, actions) => {
              actions.resetForm();
              handleAddUser(values);
            }}
          >
            {(formikProps) => (
              <View>
                <Text style={styles.text}>Ime i prezime</Text>
                <TextInput
                  style={styles.formikInput}
                  placeholder={route.params.user.name}
                  placeholderTextColor={"#C9C9D1"}
                  onChangeText={formikProps.handleChange("name")}
                  value={formikProps.values.name}
                  onBlur={formikProps.handleBlur("name")}
                />
                <Text style={styles.errorText}>
                  {formikProps.touched.name && formikProps.errors.name}
                </Text>
                <Text style={styles.text}>Adresa stanovanja</Text>
                <TextInput
                  style={styles.formikInput}
                  placeholder={route.params.user.address}
                  placeholderTextColor={"#C9C9D1"}
                  onChangeText={formikProps.handleChange("address")}
                  value={formikProps.values.address}
                  onBlur={formikProps.handleBlur("address")}
                />
                <Text style={styles.errorText}>
                  {formikProps.touched.address && formikProps.errors.address}
                </Text>
                <Text style={styles.text}>Broj mobitela</Text>
                <TextInput
                  style={styles.formikInput}
                  placeholder={route.params.user.phone}
                  placeholderTextColor={"#C9C9D1"}
                  onChangeText={formikProps.handleChange("telephone")}
                  value={formikProps.values.telephone}
                  onBlur={formikProps.handleBlur("telephone")}
                  keyboardType="numeric"
                />
                <Text style={styles.errorText}>
                  {formikProps.touched.telephone && formikProps.errors.telephone}
                </Text>
                <Text style={styles.text}>Email adresa</Text>
                <TextInput
                  style={styles.formikInput}
                  placeholder={route.params.user.email}
                  placeholderTextColor={"#C9C9D1"}
                  onChangeText={formikProps.handleChange("email")}
                  value={formikProps.values.email}
                  onBlur={formikProps.handleBlur("email")}
                />
                <Text style={styles.errorText}>
                  {formikProps.touched.email && formikProps.errors.email}
                </Text>
                <Text style={styles.text}>Datum priključka</Text>
                <TextInput
                  style={styles.formikInput}
                  placeholder={"Datum priključka"}
                  placeholderTextColor={"#C9C9D1"}
                  onChangeText={formikProps.handleChange("date")}
                  value={formikProps.values.date}
                  onBlur={formikProps.handleBlur("date")}
                  keyboardType="numeric"
                />
                <Text style={styles.errorText}>
                  {formikProps.touched.date && formikProps.errors.date}
                </Text>

                <Text style={styles.text}>Vrsta priključka</Text>
                <View style={styles.wrapperContainer}>
                  <CheckBox
                    title="Kuća"
                    iconType="material-community"
                    checkedIcon="check-circle-outline"
                    uncheckedIcon="circle-outline"
                    checkedColor="#013A89"
                    containerStyle={{
                      width: 100,
                      margin: 0,
                      padding: 0,
                    }}
                    fontFamily="montserrat400"
                    textStyle={{
                      fontSize: 14,
                      color: "#7D788E",
                      fontWeight: "400",
                    }}
                    checked={toggleCheckBox}
                    onPress={() => setToggleCheckBox(!toggleCheckBox)}
                  />
                  <CheckBox
                    title="Stan"
                    iconType="font-awesome"
                    checkedIcon="dot-circle-o"
                    uncheckedIcon="circle-thin"
                    checkedColor="#013A89"
                    containerStyle={{
                      width: 100,
                      margin: 0,
                      padding: 0,
                    }}
                    fontFamily="montserrat400"
                    textStyle={{
                      fontSize: 14,
                      color: "#7D788E",
                      fontWeight: "400",
                    }}
                    checked={toggleCheckBox}
                    onPress={() => setToggleCheckBox(!toggleCheckBox)}
                  />
                </View>
                {/* This button submits formik data, but the above Spremi promjene button needs to do that */}
                {/* <View style={styles.buttonsContainer}>
                  <View style={styles.buttonContainer}>
                    <PrimaryButton
                      title="Potvrdi"
                      onPress={formikProps.handleSubmit}
                    />
                  </View>
                </View> */}
              </View>
            )}
          </Formik>
        </ScrollView>
      </View>
    </TouchableWithoutFeedback>
  );
};

export default AddUserDetailsScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    position: "relative",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "white",
  },
  userContainer: {
    flexDirection: "row",
    marginTop: 70,
    marginBottom: 20,
    marginLeft: -7,
  },
  titleContainer: {
    marginLeft: 18,
  },
  title: {
    fontFamily: "montserrat500",
    color: "#6A657E",
    fontSize: 20,
    marginTop: 8,
    marginBottom: 18,
  },
  userIconDropdown: {
    top: 0,
    left: 217,
    zIndex: 1,
  },
  bellIconDropdown: {
    position: "absolute",
    top: 0,
    left: 27,
    zIndex: 1,
  },
  formikInput: {
    width: 358,
    height: 45,
    padding: 10,
    marginTop: 10,
    marginBottom: 25,
    borderWidth: 1,
    borderColor: "#ccc",
    fontSize: 18,
    borderRadius: 5,
    borderColor: "#D8D6DE",
  },
  formContainer: {
    backgroundColor: "#fff",
    margin: 20,
    padding: 18,
    borderRadius: 8,
    width: "86%",
    elevation: 6,
  },
  wrapperContainer: {
    flexDirection: "row",
    justifyContent: "flex-start",
    marginTop: 35,
    marginBottom: 50,
    marginLeft: -9,
  },
  buttonsContainer: {
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
    marginVertical: 14,
  },
  buttonContainer: {
    marginRight: 41,
  },
  text: {
    fontFamily: "montserrat400",
    color: "#6A657E",
    fontSize: 16,
    alignSelf: "flex-start",
  },
  errorText: {
    color: "red",
    marginTop: -18,
  },
});
