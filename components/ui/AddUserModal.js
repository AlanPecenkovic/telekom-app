import React, { useState } from "react";
import {
  Keyboard,
  Modal,
  StyleSheet,
  Text,
  TextInput,
  TouchableWithoutFeedback,
  View,
} from "react-native";
import { Formik } from "formik";
import * as yup from "yup";
import { CheckBox } from "@rneui/base";
import PrimaryButton from "./PrimaryButton";
import SecondaryButton from "./SecondaryButton";

const addUserSchema = yup.object({
  name: yup.string().required("Ime i prezime je obavezno unijeti").min(3),
  address: yup.string().required("Unesite adresu stanovanja").min(4),
  phone: yup.number().integer().required("Unesite broj telefona"),
  email: yup.string().email("Molim Vas unesite ispravnu email adresu"),
  date: yup.date().max(new Date()),
});

const AddUserModal = ({ modalVisible, handleAddUserModal, dismissModal }) => {
  const [toggleCheckBox, setToggleCheckBox] = useState(false);

  return (
    <Modal animationType="slide" visible={modalVisible} transparent={false}>
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <View style={styles.container}>
          <Formik
            initialValues={{
              name: "",
              address: "",
              phone: "",
              email: "",
              date: "",
            }}
            validationSchema={addUserSchema}
            onSubmit={(values, actions) => {
              actions.resetForm();
              handleAddUserModal(values);
            }}
          >
            {(formikProps) => (
              <View>
                <Text style={styles.text}>Ime i prezime</Text>
                <TextInput
                  style={styles.formikInput}
                  placeholder={"Ime i prezime"}
                  placeholderTextColor={"#C9C9D1"}
                  onChangeText={formikProps.handleChange("name")}
                  value={formikProps.values.name}
                  onBlur={formikProps.handleBlur("name")}
                />
                <Text style={styles.errorText}>
                  {formikProps.touched.name && formikProps.errors.name}
                </Text>
                <Text style={styles.text}>Adresa stanovanja</Text>
                <TextInput
                  style={styles.formikInput}
                  placeholder={"Adresa stanovanja"}
                  placeholderTextColor={"#C9C9D1"}
                  onChangeText={formikProps.handleChange("address")}
                  value={formikProps.values.address}
                  onBlur={formikProps.handleBlur("adress")}
                />
                <Text style={styles.errorText}>
                  {formikProps.touched.address && formikProps.errors.address}
                </Text>
                <Text style={styles.text}>Broj mobitela</Text>
                <TextInput
                  style={styles.formikInput}
                  placeholder={"Broj mobitela"}
                  placeholderTextColor={"#C9C9D1"}
                  onChangeText={formikProps.handleChange("phone")}
                  value={formikProps.values.phone}
                  onBlur={formikProps.handleBlur("phone")}
                  keyboardType="numeric"
                />
                <Text style={styles.errorText}>
                  {formikProps.touched.phone && formikProps.errors.phone}
                </Text>
                <Text style={styles.text}>Email adresa</Text>
                <TextInput
                  style={styles.formikInput}
                  placeholder={"Email adresa"}
                  placeholderTextColor={"#C9C9D1"}
                  onChangeText={formikProps.handleChange("email")}
                  value={formikProps.values.email}
                  onBlur={formikProps.handleBlur("email")}
                />
                <Text style={styles.errorText}>
                  {formikProps.touched.email && formikProps.errors.email}
                </Text>
                <Text style={styles.text}>Datum priključka</Text>
                <TextInput
                  style={styles.formikInput}
                  placeholder={"Datum priključka"}
                  placeholderTextColor={"#C9C9D1"}
                  onChangeText={formikProps.handleChange("date")}
                  value={formikProps.values.date}
                  onBlur={formikProps.handleBlur("date")}
                  keyboardType="numeric"
                />
                <Text style={styles.errorText}>
                  {formikProps.touched.date && formikProps.errors.date}
                </Text>

                <Text style={styles.text}>Vrsta priključka</Text>
                <View style={styles.wrapperContainer}>
                  <CheckBox
                    title="Kuća"
                    iconType="material-community"
                    checkedIcon="check-circle-outline"
                    uncheckedIcon="circle-outline"
                    checkedColor="#013A89"
                    containerStyle={{
                      width: 100,
                      margin: 0,
                      padding: 0,
                    }}
                    fontFamily="montserrat400"
                    textStyle={{
                      fontSize: 14,
                      color: "#7D788E",
                      fontWeight: "400",
                    }}
                    checked={toggleCheckBox}
                    onPress={() => setToggleCheckBox(!toggleCheckBox)}
                  />
                  <CheckBox
                    title="Stan"
                    iconType="font-awesome"
                    checkedIcon="dot-circle-o"
                    uncheckedIcon="circle-thin"
                    checkedColor="#013A89"
                    containerStyle={{
                      width: 100,
                      margin: 0,
                      padding: 0,
                    }}
                    fontFamily="montserrat400"
                    textStyle={{
                      fontSize: 14,
                      color: "#7D788E",
                      fontWeight: "400",
                    }}
                    checked={toggleCheckBox}
                    onPress={() => setToggleCheckBox(!toggleCheckBox)}
                  />
                </View>

                <View style={styles.buttonsContainer}>
                  <View style={styles.buttonContainer}>
                    <PrimaryButton
                      title="Potvrdi"
                      onPress={formikProps.handleSubmit}
                    />
                  </View>
                  <View style={styles.buttonContainer}>
                    <SecondaryButton
                      title="Poništi"
                      color="grey"
                      onPress={dismissModal}
                    />
                  </View>
                </View>
              </View>
            )}
          </Formik>
        </View>
      </TouchableWithoutFeedback>
    </Modal>
  );
};

export default AddUserModal;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#fff",
  },
  formikInput: {
    width: 358,
    height: 45,
    padding: 10,
    marginTop: 10,
    marginBottom: 25,
    borderWidth: 1,
    borderColor: "#ccc",
    fontSize: 18,
    borderRadius: 5,
    borderColor: "#D8D6DE",
  },
  formContainer: {
    backgroundColor: "#fff",
    margin: 20,
    padding: 18,
    borderRadius: 8,
    width: "86%",
    elevation: 6,
  },
  wrapperContainer: {
    flexDirection: "row",
    justifyContent: "flex-start",
    marginTop: 35,
    marginBottom: 9,
    margin: -9,
  },
  buttonsContainer: {
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
    marginVertical: 14,
  },
  buttonContainer: {
    marginRight: 41,
  },
  text: {
    fontFamily: "montserrat400",
    color: "#6A657E",
    fontSize: 16,
    alignSelf: "flex-start",
  },
  errorText: {
    color: "red",
    marginTop: -18,
  },
});
