import React from "react";
import { Pressable, StyleSheet, Text, View } from "react-native";

const LoginButton = (props) => {
  return (
    <View style={styles.buttonContainer}>
      <Pressable onPress={props.onPress} android_ripple={{ color: "#63c5ec" }}>
        <Text style={styles.text}>{props.children}</Text>
      </Pressable>
    </View>
  );
};

export default LoginButton;

const styles = StyleSheet.create({
  buttonContainer: {
    flex: 1,
    borderRadius: 6,
    backgroundColor: "#FFFFFF",
    justifyContent: "center",
  },
  text: {
    fontFamily: "montserrat500",
    fontSize: 16,
    color: "#7D788E",
    textAlign: "center",
  },
});
