import { Button } from "@rneui/base";
import React from "react";

const PrimaryButton = ({ onPress, title }) => {
  return (
    <Button
      title={title}
      onPress={onPress}
      buttonStyle={{ width: 100, height: 45 }}
      titleStyle={{
        fontFamily: "montserrat600",
        fontSize: 16,
      }}
      color="#013A89"
      radius={5}
    />
  );
};

export default PrimaryButton;
