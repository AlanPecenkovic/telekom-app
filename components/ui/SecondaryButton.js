import { Button } from "@rneui/base";
import React from "react";

const SecondaryButton = ({ onPress, title }) => {
  return (
    <Button
      title={title}
      onPress={onPress}
      buttonStyle={{
        width: 100,
        height: 45,
        borderWidth: 1,
        borderColor: "#D8D6DE",
        backgroundColor: "#fff",
      }}
      titleStyle={{
        color: "#7D788E",
        fontFamily: "montserrat600",
      }}
      color="#D8D6DE"
      radius={5}
      type="outline"
    />
  );
};

export default SecondaryButton;
