import { Icon } from "@rneui/base";
import React from "react";
import { Pressable, StyleSheet, Text, View } from "react-native";

const ShowDropdown = ({ onPress, title, containerStyle, innerContainer }) => {
  return (
    <View style={[styles.container, containerStyle]}>
      <Text style={styles.text}>{title}</Text>
      <Pressable onPress={onPress}>
        <View style={[styles.innerContainer, innerContainer]}>
          <Text style={styles.number}>10</Text>
          <Icon
            name="chevron-down"
            type="evilicon"
            size={32}
            color="#a6a3af"
            style={styles.icon}
          />
        </View>
      </Pressable>
    </View>
  );
};

export default ShowDropdown;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "row",
    width: 350,
    height: 76,
    justifyContent: "flex-start",
    alignItems: "center",
    alignSelf: "center",
    backgroundColor: "#fff",
    marginTop: 50,
    marginBottom: 8,
    borderRadius: 10,
    elevation: 6,
  },
  innerContainer: {
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
    width: 80,
    height: 36,
    marginTop: 29,
    marginBottom: 26,
    backgroundColor: "#fff",
    borderRadius: 5,
    borderWidth: 1,
    borderRadius: 5,
    borderColor: "#D8D6DE",
  },
  text: {
    paddingRight: 10,
    paddingLeft: 20,
    fontFamily: "montserrat400",
    fontSize: 14,
    color: "#7D788E",
  },
  number: {
    paddingLeft: 12,
    fontFamily: "montserrat400",
    fontSize: 14,
    color: "#7D788E",
  },
  icon: {
    paddingLeft: 16,
  },
});
