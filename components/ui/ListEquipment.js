import React from "react";
import { FlatList, Text, View } from "react-native";
import { StyleSheet } from "react-native";
import ListTitle from "./ListTitle";

const ListEquipment = ({ userData, title }) => {
  return (
    <View style={styles.outerContainer}>
      <ListTitle title={title} />
      <View style={styles.innerContainer}>
        <View style={styles.titleContainer}>
          <Text style={styles.titleText}>ID</Text>
          <Text style={styles.secondtitleText}>Oprema</Text>
          <Text style={styles.titleText}>Zaduženo</Text>
        </View>
        <FlatList
          data={userData}
          keyExtractor={(data) => data.id}
          renderItem={(data) => {
            return (
              <View style={styles.textContainer}>
                <View style={styles.idWrapper}>
                  <Text style={styles.id}>{data.item.id}</Text>
                </View>
                <View style={styles.nameWrapper}>
                  <Text style={styles.text}>{data.item.username}</Text>
                </View>
                <Text style={styles.text}>{data.item.id}</Text>
              </View>
            );
          }}
        />
      </View>
    </View>
  );
};

export default ListEquipment;

const styles = StyleSheet.create({
  outerContainer: {
    flex: 1,
    // justifyContent: "center",
    alignItems: "center",
    backgroundColor: "white",
    paddingBottom: 30,
    minHeight: 500,
  },
  innerContainer: {
    width: "90%",
    backgroundColor: "white",
    elevation: 8,
    borderRadius: 10,
    overflow: "hidden",
  },
  titleContainer: {
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
    paddingHorizontal: 26,
    backgroundColor: "#F0F6FF",
    width: "100%",
    height: 38,
  },
  titleText: {
    fontFamily: "montserrat400",
    color: "#6A657E",
    paddingRight: 48,
  },
  secondtitleText: {
    fontFamily: "montserrat400",
    color: "#6A657E",
    paddingRight: 100,
  },
  textContainer: {
    flexDirection: "row",
    padding: 20,
    paddingHorizontal: 30,
    borderWidth: 1,
    borderColor: "#F1F1F3",
  },
  id: {
    fontFamily: "montserrat400",
    color: "#013A89",
    fontSize: 14,
  },
  idWrapper: {
    width: 38,
    marginRight: 20,
  },
  text: {
    fontFamily: "montserrat400",
    color: "#6A657E",
    fontSize: 14,
    paddingRight: 36,
  },
  nameWrapper: {
    paddingRight: 10,
    width: 200,
  },
});
