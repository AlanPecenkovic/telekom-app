import React from "react";
import { FlatList, Pressable, Text, View } from "react-native";
import { StyleSheet } from "react-native";
import ListTitle from "./ListTitle";

const ListComponent = ({ userData, title, onPress }) => {
  return (
    <View style={styles.outerContainer}>
      <ListTitle title={title} />
      <View style={styles.innerContainer}>
        <View style={styles.titleContainer}>
          <Text style={styles.titleText}>ID</Text>
          <Text style={styles.titleText}>Korisnik</Text>
        </View>
        <FlatList
          data={userData}
          keyExtractor={(data) => data.id}
          renderItem={(data) => {
            return (
              <View style={styles.textContainer}>
                <View style={styles.idWrapper}>
                  <Text style={styles.id}>{data.item.id}</Text>
                </View>
                <Pressable onPress={() => onPress(data.item)}>
                  <Text style={styles.text}>{data.item?.name}</Text>
                  <Text style={styles.text}>{data.item.address?.city}</Text>
                </Pressable>
              </View>
            );
          }}
        />
      </View>
    </View>
  );
};

export default ListComponent;

const styles = StyleSheet.create({
  outerContainer: {
    flex: 1,
    // justifyContent: "center",
    alignItems: "center",
    backgroundColor: "white",
    paddingBottom: 30,
    minHeight: 500,
  },
  innerContainer: {
    width: "90%",
    backgroundColor: "white",
    elevation: 8,
    borderRadius: 10,
    overflow: "hidden",
  },
  titleContainer: {
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
    paddingHorizontal: 30,
    backgroundColor: "#F0F6FF",
    width: "100%",
    height: 40,
  },
  titleText: {
    fontFamily: "montserrat400",
    color: "#6A657E",
    paddingRight: 54,
  },
  textContainer: {
    flexDirection: "row",
    padding: 20,
    paddingHorizontal: 30,
    borderWidth: 1,
    borderColor: "#F1F1F3",
  },
  idWrapper: {
    width: 50,
    marginRight: 22,
  },
  id: {
    fontFamily: "montserrat400",
    color: "#013A89",
    fontSize: 14,
    paddingRight: 35,
  },
  text: {
    fontFamily: "montserrat400",
    color: "#6A657E",
    fontSize: 14,
    paddingRight: 36,
  },
});
