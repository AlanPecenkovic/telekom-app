import React from "react";
import { Image, View } from "react-native";

const MapIcon = () => {
  return (
    <View>
      <Image
        source={require("../../assets/images/map.png")}
        resizeMode="cover"
        style={{ width: 24, height: 24 }}
      />
    </View>
  );
};

export default MapIcon;
