import React, { useContext, useState } from "react";
import { StyleSheet, Text, View } from "react-native";
import { Button, CheckBox, Input } from "@rneui/themed";
import { useNavigation } from "@react-navigation/native";
import axios from "axios";
import { apiEndpoint } from "../../data/apiEndpoint";
import { UserListContext } from "../../store/userListContext";

const LoginForm = () => {
  const [toggleCheckBox, setToggleCheckBox] = useState(false);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [emailErrorMessage, setemailErrorMessage] = useState("");
  const [passwordErrorMessage, setPasswordErrorMessage] = useState("");
  const [borderColor, setborderColor] = useState("#D8D6DE");
  const [credentials, setCredentials] = useState(false);

  const navigation = useNavigation();
  const saveToken = useContext(UserListContext);
  const saveUser = useContext(UserListContext);
  const saveId = useContext(UserListContext);

  const goToForgetPassword = () => {
    navigation.navigate("ForgetPassword");
  };

  const emailIsValid = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email);

  const passwordIsValid = /^[A-Za-z]\w{6,99}$/.test(password);

  let token = "";
  let user_id = "";
  let userData = "";

  // How to give feedback onBlur and not initially...

  const handleLogin = () => {
    if (emailIsValid) {
      setemailErrorMessage("");
      setborderColor("#D8D6DE");
    } else {
      setemailErrorMessage("Unesite ispravnu email adresu!");
      setborderColor("red");
    }
    if (passwordIsValid) {
      setPasswordErrorMessage("");
      setborderColor("#D8D6DE");
    } else {
      setPasswordErrorMessage("Lozinka treba biti duža od 6 znakova");
      setborderColor("red");
    }
    // if (emailIsValid && passwordIsValid) {
    if (emailIsValid && passwordIsValid) {
      axios
        .post(apiEndpoint + "api/login", { email: email, password: password })
        .then((response) => {
          token = response.data.token;
          userData = response.data;
          user_id = userData.user.id;
          saveToken.saveToken(token);
          saveUser.saveUserData(userData);
          saveId.saveId(user_id);
          navigation.navigate("BottomTabs", {
            screen: "EmployeeAdmin",
            params: { token: token, userData: userData },
          });
        })
        .catch((error) => {
          if (error.response.data.action === "ERROR") {
            setCredentials(true);
          }
        });
    } else {
      console.log("Enter the correct email/password.");
      setCredentials(false);
    }
  };

  function handleEmail(enteredEmail) {
    setEmail(enteredEmail);
  }

  function handlePassword(enteredPassword) {
    setPassword(enteredPassword);
  }

  const resetLoginForm = () => {
    console.log("Login form reset!");
  };

  return (
    <View style={styles.outerContainer}>
      <View style={styles.formContainer}>
        <Text style={styles.title}>Prijava</Text>
        <Input
          label="Email"
          labelStyle={{
            color: "#7D788E",
            fontWeight: "400",
            fontSize: 16,
            fontFamily: "montserrat400",
            marginVertical: 8,
          }}
          placeholder="Email"
          leftIcon={{
            type: "octicon",
            name: "mail",
            size: 22,
            color: "#6E6B7B",
            paddingRight: 10,
          }}
          // leftIconContainerStyle={{ backgroundColor: "blue" }}
          containerStyle={{ marginHorizontal: -9 }}
          inputContainerStyle={{
            width: 300,
            height: 45,
            borderWidth: 1,
            borderColor: borderColor,
            borderRadius: 4,
            padding: 10,
          }}
          inputStyle={{
            fontSize: 16,
            fontFamily: "montserrat400",
          }}
          onChangeText={handleEmail}
          value={email}
          errorMessage={emailErrorMessage}
          errorStyle={{ fontSize: 13 }}
          // onTouchEnd={} possible use of this in validation error messages
        />

        <Input
          label="Lozinka"
          labelStyle={{
            color: "#7D788E",
            fontWeight: "400",
            fontSize: 16,
            fontFamily: "montserrat400",
            marginVertical: 8,
          }}
          placeholder="Lozinka"
          leftIcon={{
            type: "octicon",
            name: "lock",
            size: 22,
            color: "#6E6B7B",
            paddingRight: 10,
          }}
          containerStyle={{ marginHorizontal: -9 }}
          inputContainerStyle={{
            width: 300,
            height: 45,
            borderWidth: 1,
            borderColor: borderColor,
            borderRadius: 4,
            padding: 10,
          }}
          inputStyle={{
            fontSize: 16,
            fontFamily: "montserrat400",
            color: "red",
          }}
          secureTextEntry={true}
          onChangeText={handlePassword}
          value={password}
          errorMessage={passwordErrorMessage}
          errorStyle={{ fontSize: 13 }}
        />

        {credentials ? (
          <Text style={styles.errorText}>Unesite ispravan email/lozinku!</Text>
        ) : null}

        <View style={styles.wrapperContainer}>
          <CheckBox
            title="Zapamti me"
            containerStyle={{
              width: 120,
              margin: 0,
              padding: 0,
            }}
            fontFamily="montserrat400"
            textStyle={{
              fontSize: 14,
              color: "#7D788E",
              fontWeight: "400",
            }}
            checked={toggleCheckBox}
            onPress={() => setToggleCheckBox(!toggleCheckBox)}
          />
          <Text style={styles.smallText} onPress={goToForgetPassword}>
            Zaboravljena lozinka?
          </Text>
        </View>

        <View style={styles.buttonsContainer}>
          <View style={styles.buttonContainer}>
            <Button
              title="Potvrdi"
              onPress={handleLogin}
              buttonStyle={{ width: 100, height: 45 }}
              titleStyle={{
                fontFamily: "montserrat600",
                fontSize: 16,
              }}
              color="#013A89"
              radius={5}
            />
          </View>
          <View style={styles.buttonContainer}>
            <Button
              title="Poništi"
              onPress={resetLoginForm}
              buttonStyle={{
                width: 100,
                height: 45,
                borderWidth: 1,
                borderColor: "#D8D6DE",
              }}
              titleStyle={{
                color: "#7D788E",
                fontFamily: "montserrat600",
              }}
              color="#D8D6DE"
              radius={5}
              type="outline"
            />
          </View>
        </View>
      </View>
    </View>
  );
};

export default LoginForm;

const styles = StyleSheet.create({
  outerContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#fff",
  },
  formContainer: {
    backgroundColor: "#fff",
    margin: 20,
    padding: 18,
    borderRadius: 8,
    width: "86%",
    elevation: 6,
  },
  title: {
    color: "#6A657E",
    fontFamily: "montserrat500",
    fontSize: 20,
    fontWeight: "500",
    marginBottom: 16,
  },
  text: {
    color: "#7D788E",
    fontFamily: "montserrat400",
    fontSize: 16,
    marginVertical: 10,
  },
  smallText: {
    color: "#7D788E",
    fontFamily: "montserrat400",
    fontSize: 14,
    marginLeft: 20,
  },
  errorText: {
    color: "red",
    fontFamily: "montserrat400",
    fontSize: 14,
    marginLeft: 20,
  },
  input: {
    width: 300,
    height: 45,
    borderWidth: 1,
    borderColor: "#D8D6DE",
    borderRadius: 4,
    padding: 6,
    marginBottom: 10,
  },
  wrapperContainer: {
    flexDirection: "row",
    alignItems: "center",
    margin: -9,
    marginVertical: 10,
  },
  buttonsContainer: {
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
    marginVertical: 10,
  },
  buttonContainer: {
    borderRadius: 18,
    marginRight: 14,
  },
});
