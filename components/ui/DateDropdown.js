import { Icon } from "@rneui/base";
import React from "react";
import { Pressable, StyleSheet, Text, View } from "react-native";

const DateDropdown = ({ onPress, title }) => {
  return (
    <View style={styles.container}>
      <View style={styles.innerContainer}>
        <Pressable onPress={onPress} style={styles.pressable}>
          <Text style={styles.text}>{title}</Text>
          <Icon
            name="chevron-down"
            type="evilicon"
            size={34}
            color="#a6a3af"
            style={styles.icon}
          />
        </Pressable>
      </View>
    </View>
  );
};

export default DateDropdown;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: 350,
    height: 91,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#fff",
    marginVertical: 15,
    borderRadius: 10,
    elevation: 6,
  },
  innerContainer: {
    flex: 1,
    marginTop: 29,
    marginBottom: 26,
    backgroundColor: "#fff",
    borderRadius: 5,
    borderWidth: 1,
    borderRadius: 5,
    borderColor: "#D8D6DE",
    width: "86%",
  },
  pressable: {
    flexDirection: "row",
  },
  text: {
    paddingVertical: 6,
    paddingLeft: 25,
    fontFamily: "montserrat400",
    fontSize: 16,
    color: "#7D788E",
  },
  icon: {
    paddingTop: 3,
    paddingLeft: 112,
  },
});
