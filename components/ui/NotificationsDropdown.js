import { useNavigation } from "@react-navigation/native";
import { Button, Icon } from "@rneui/base";
import React from "react";
import { FlatList, StyleSheet, Text, View } from "react-native";

const NotificationsDropdown = ({ style }) => {
  const navigation = useNavigation();
  function toNotificationsScreen() {
    // navigation.navigate("Notifications");
    navigation.navigate("Profile", {
      showNotifications: true,
    });
  }
  const DUMMY_NOTIFICATIONS = [
    {
      id: 1,
      type: "user",
      description: "Potencijalni korisnik",
    },
    {
      id: 2,
      type: "user",
      description: "Novi korisnik",
    },
    {
      id: 3,
      type: "alert-triangle",
      description: "Minimalna količina",
    },
    {
      id: 4,
      type: "alert-triangle",
      description: "Nema na stanju",
    },
    {
      id: 5,
      type: "user",
      description: "Radnik 1 je izvršio nalog",
    },
  ];
  return (
    <View style={[styles.container, style]}>
      <Text style={styles.title}>Obavijesti</Text>
      <FlatList
        // nestedScrollEnabled
        // style={{ flex: 1 }}
        contentContainerStyle={{
          paddingVertical: 20,
        }}
        data={DUMMY_NOTIFICATIONS}
        keyExtractor={(item) => item.id}
        renderItem={(userData) => {
          return (
            <View style={styles.innerContainer}>
              <Icon
                raised
                reverse
                name={userData.item.type}
                type="feather"
                size={18}
                color={
                  userData.item.type === "alert-triangle"
                    ? "#f0a6a6"
                    : "#F5F5F5"
                }
                iconStyle={{ color: "#6A657E" }}
              />
              <Text style={styles.text}>{userData.item.description}</Text>
            </View>
          );
        }}
      />
      <View style={styles.buttonContainer}>
        <Button
          title="Sve obavijesti"
          titleStyle={{ fontSize: 16, fontFamily: "montserrat600" }}
          buttonStyle={{
            backgroundColor: "#013A89",
            borderRadius: 5,
            width: 240,
            height: 45,
          }}
          containerStyle={{
            alignSelf: "center",
          }}
          onPress={toNotificationsScreen}
        />
      </View>
    </View>
  );
};

export default NotificationsDropdown;

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    // flexGrow: 1,
    width: 316,
    height: 434,
    borderRadius: 5,
    backgroundColor: "white",
    position: "absolute",
    top: 60,
    left: -74,
    elevation: 6,
  },
  innerContainer: {
    alignItems: "center",
    flexDirection: "row",
    borderColor: "#F1F1F3",
    borderWidth: 1,
    paddingLeft: 15,
    paddingVertical: 5,
  },
  title: {
    fontFamily: "montserrat500",
    fontSize: 14,
    color: "#7D788E",
    paddingTop: 20,
    marginBottom: -6,
    marginLeft: 20,
  },
  text: {
    fontFamily: "montserrat400",
    fontSize: 14,
    color: "#7D788E",
    paddingVertical: 12,
    marginLeft: 12,
  },
  buttonContainer: {
    paddingVertical: 6,
    borderTopWidth: 0.7,
    borderTopColor: "#F1F1F3",
  },
});
