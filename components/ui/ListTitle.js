import React from "react";
import { Text } from "react-native";

const ListTitle = ({ title }) => {
  return (
    <Text
      style={{
        fontFamily: "montserrat500",
        fontSize: 14,
        marginLeft: 3,
        alignSelf: "flex-start",
        marginLeft: 20,
        marginBottom: 10,
        color: "#6A657E",
      }}
    >
      {title}
    </Text>
  );
};

export default ListTitle;
