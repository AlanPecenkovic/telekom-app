import React from "react";
import { StyleSheet, Text, View } from "react-native";

const EmployeeOrderComponent = ({ id, name, address }) => {
  return (
    <View style={styles.container}>
      <View style={styles.userInfo}>
        <Text style={styles.userId}>{id}</Text>
        <Text style={styles.user}>{name}</Text>
      </View>
      <View style={styles.userAdresss}>
        <Text style={styles.street}>{address.street}</Text>
        <Text style={styles.city}>{address.city}</Text>
        <Text style={styles.zipcode}>{address.zipcode}</Text>
      </View>
    </View>
  );
};

export default EmployeeOrderComponent;

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#fff",
    justifyContent: "center",
    alignItems: "flex-start",
    width: 350,
    height: 112,
    marginTop: 50,
    marginBottom: 25,
    borderRadius: 10,
    elevation: 6,
  },
  userInfo: {
    flexDirection: "row",
    // flexWrap: "wrap",
  },
  userAdresss: {
    flexDirection: "row",
    // flexWrap: "wrap",
    marginLeft: -25,
  },
  userId: {
    color: "#013A89",
    fontFamily: "montserrat400",
    fontSize: 14,
    marginLeft: 25,
  },
  user: {
    fontFamily: "montserrat400",
    fontSize: 14,
    marginLeft: 50,
    color: "#6A657E",
    marginBottom: 25,
  },
  street: {
    fontFamily: "montserrat400",
    fontSize: 16,
    marginLeft: 50,
    color: "#6A657E",
  },
  city: {
    fontFamily: "montserrat400",
    fontSize: 16,
    marginLeft: 10,
    color: "#6A657E",
  },
  zipcode: {
    fontFamily: "montserrat400",
    fontSize: 16,
    marginLeft: 10,
    color: "#6A657E",
  },
});
