import React, { useRef, useState, useContext, useEffect } from "react";
import { Button, Icon } from "@rneui/base";
import {
  Image,
  Keyboard,
  KeyboardAvoidingView,
  Modal,
  Platform,
  StyleSheet,
  Text,
  TextInput,
  TouchableWithoutFeedback,
  View,
} from "react-native";
import { Formik } from "formik";
import * as yup from "yup";
import * as ImagePicker from "expo-image-picker";
import axios from "axios";
import PrimaryButton from "./PrimaryButton";
import SecondaryButton from "./SecondaryButton";
import SelectList from "react-native-dropdown-select-list";
import { UserListContext } from "../../store/userListContext";
import { apiEndpoint } from "../../data/apiEndpoint";

const WorkEntryModal = ({ modalVisible, dismissModal }) => {
  const [selectedArticle, setSelectedArticle] = useState("");
  const [selected, setSelected] = useState("");
  const [image, setImage] = useState(null);
  const usersCtx = useContext(UserListContext);

  const data = [
    { key: "1", value: "Optički kabal" },
    { key: "2", value: "Konektori" },
    { key: "3", value: "Kliješta" },
  ];

  const pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1,
    });

    if (!result.cancelled) {
      setImage(result.uri);
    }
  };

  // function handlePress() {
  //   console.log("user clicked");
  // }

  const addUserSchema = yup.object({
    selectedArticleAmount: yup.number().required("Unesite količinu artikla").max(99),
    electricSocket: yup.string().required("Unesite mjerenje utičnice").min(4),
    measureODF: yup.string().required("Unesite mjerenje ODF centar").min(3),
    photo: yup.string().nullable(),
    // photo: yup.object().required("Molimo Vas odaberite sliku").nullable(),
  });

  // useEffect(() => {
  //   return () => {
  //     isApiSubscribed = false;
  //   };
  // }, []);

  return (
    <KeyboardAvoidingView behavior={Platform.OS === "ios" ? "padding" : "height"}>
      <Modal
        animationType="slide"
        visible={modalVisible}
        transparent={false}
        statusBarTranslucent={true}
      >
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
          <View style={styles.container}>
            <View style={styles.titleContainer}>
              <View style={styles.innnerTitleContainer}>
                <Text style={styles.titleText}>Upis podataka</Text>
                <Icon name="x" type="feather" color="#7D788E" onPress={dismissModal} />
              </View>
            </View>

            <View style={styles.formContainer}>
              <Formik
                initialValues={{
                  // artikli: "",
                  selectedArticleAmount: "",
                  electricSocket: "",
                  measureODF: "",
                  photo: null,
                }}
                validationSchema={addUserSchema}
                onSubmit={(values, actions) => {
                  actions.resetForm();

                  const headersConfig = {
                    Authorization: `Bearer ${usersCtx.token}`,
                    "Content-Type": "application/json",
                  };
                  axios({
                    method: "post",
                    url: apiEndpoint + "api/daily-tasks/finish",
                    data: { id: usersCtx.user_id },
                    headers: headersConfig,
                  })
                    .then((response) => {
                      console.log(response.data.response);
                    })
                    .catch((error) => console.log(error));
                  console.log(
                    values.selectedArticleAmount,
                    values.electricSocket,
                    values.measureODF
                  );
                }}
              >
                {(formikProps) => (
                  <View>
                    <Text style={styles.text}>Korišteni artikli</Text>
                    <SelectList
                      setSelected={setSelected}
                      data={data}
                      onSelect={() => console.log(data.value)}
                      boxStyles={{
                        marginTop: 10,
                        marginBottom: 25,
                        borderWidth: 1,
                        borderColor: "#D8D6DE",
                        borderRadius: 5,
                      }}
                      inputStyles={{
                        fontFamily: "montserrat400",
                        fontSize: 16,
                        color: "#C9C9D1",
                      }}
                      dropdownStyles={{
                        borderWidth: 1,
                        borderColor: "#D8D6DE",
                        borderRadius: 10,
                      }}
                      dropdownTextStyles={{
                        fontFamily: "montserrat400",
                        fontSize: 14,
                        color: "#6A657E",
                        marginVertical: 5,
                        paddingLeft: 3,
                      }}
                      placeholder="Korišteni artikli"
                      search={false}
                    />
                    {selected ? (
                      <View style={styles.selectedArticleAmountView}>
                        <Text style={[styles.text, { marginTop: 14 }]}>
                          {data[selected - 1].value}
                        </Text>
                        <TextInput
                          style={styles.articleAmount}
                          onChangeText={formikProps.handleChange(
                            "selectedArticleAmount"
                          )}
                          value={formikProps.values.selectedArticleAmount}
                          onBlur={formikProps.handleBlur("selectedArticleAmount")}
                          keyboardType="numeric"
                        />
                      </View>
                    ) : null}
                    <Text style={[styles.errorText, { alignSelf: "flex-end" }]}>
                      {formikProps.touched.selectedArticleAmount &&
                        formikProps.errors.selectedArticleAmount}
                    </Text>

                    <Text style={styles.text}>Mjerenje utičnice</Text>
                    <TextInput
                      style={styles.formikInput}
                      placeholder={"Mjerenje utičnice"}
                      placeholderTextColor={"#C9C9D1"}
                      onChangeText={formikProps.handleChange("electricSocket")}
                      value={formikProps.values.electricSocket}
                      onBlur={formikProps.handleBlur("electricSocket")}
                    />
                    <Text style={styles.errorText}>
                      {formikProps.touched.electricSocket &&
                        formikProps.errors.electricSocket}
                    </Text>

                    <Text style={styles.text}>Mjerenje ODF centar</Text>
                    <TextInput
                      style={styles.formikInput}
                      placeholder={"Mjerenje ODF centar"}
                      placeholderTextColor={"#C9C9D1"}
                      onChangeText={formikProps.handleChange("measureODF")}
                      value={formikProps.values.measureODF}
                      onBlur={formikProps.handleBlur("measureODF")}
                    />
                    <Text style={styles.errorText}>
                      {formikProps.touched.measureODF && formikProps.errors.measureODF}
                    </Text>

                    <Text style={styles.text}>Odaberi datoteku</Text>
                    <View style={styles.fileInput}>
                      <Button
                        containerStyle={{
                          borderLeftWidth: 1,
                          borderLeftColor: "#D8D6DE",
                          width: 87,
                          height: 45,
                          alignSelf: "flex-end",
                        }}
                        buttonStyle={{
                          backgroundColor: "#fff",
                        }}
                        title="Odaberi"
                        titleStyle={{
                          fontFamily: "montserrat400",
                          fontSize: 16,
                          color: "#7D788E",
                          paddingTop: 4,
                        }}
                        onPress={pickImage}
                      />
                      {image && (
                        <Image
                          source={{ uri: image }}
                          style={{ width: 200, height: 200 }}
                        />
                      )}
                    </View>
                    <Text style={styles.errorText}>
                      {formikProps.touched.photo && formikProps.errors.photo}
                    </Text>

                    <View style={styles.buttonsContainer}>
                      <View style={styles.buttonContainer}>
                        <PrimaryButton
                          title="Potvrdi"
                          onPress={() => {
                            formikProps.handleSubmit();
                            dismissModal();
                          }}
                        />
                      </View>
                      <View>
                        <SecondaryButton
                          title="Poništi"
                          color="grey"
                          onPress={dismissModal}
                        />
                      </View>
                    </View>
                  </View>
                )}
              </Formik>
            </View>
          </View>
        </TouchableWithoutFeedback>
      </Modal>
    </KeyboardAvoidingView>
  );
};

export default WorkEntryModal;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "flex-start",
    alignItems: "center",
    backgroundColor: "#fff",
  },
  titleContainer: {
    justifyContent: "flex-end",
    width: "100%",
    height: 99,
    backgroundColor: "#F6F6F6",
  },
  innnerTitleContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingHorizontal: 25,
    marginBottom: 19,
  },
  titleText: {
    color: "#6A657E",
    fontFamily: "montserrat500",
    fontSize: 14,
  },
  formContainer: {
    // flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#fff",
    marginTop: 25,
  },
  pickerContainer: {
    width: 358,
    borderWidth: 1,
    borderColor: "#ccc",
    borderRadius: 5,
    overflow: "hidden",
  },
  selectedArticleAmountView: {
    flexDirection: "row",
    width: 358,
    justifyContent: "space-between",
  },
  articleAmount: {
    width: 90,
    height: 45,
    paddingHorizontal: 22,
    marginBottom: 25,
    borderWidth: 1,
    borderColor: "#D8D6DE",
    fontFamily: "montserrat400",
    fontSize: 16,
    borderRadius: 5,
    borderColor: "#D8D6DE",
  },
  formikInput: {
    width: 358,
    height: 45,
    padding: 10,
    marginTop: 10,
    marginBottom: 25,
    borderWidth: 1,
    borderColor: "#D8D6DE",
    fontFamily: "montserrat400",
    fontSize: 16,
    borderRadius: 5,
    borderColor: "#D8D6DE",
  },
  fileInput: {
    width: 358,
    height: 45,
    marginTop: 10,
    marginBottom: 25,
    borderWidth: 1,
    borderColor: "#D8D6DE",
    fontFamily: "montserrat400",
    fontSize: 16,
    borderRadius: 5,
    borderColor: "#D8D6DE",
    overflow: "hidden",
  },
  buttonsContainer: {
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
  },
  buttonContainer: {
    marginRight: 25,
  },
  text: {
    fontFamily: "montserrat400",
    color: "#6A657E",
    fontSize: 16,
  },
  errorText: {
    color: "red",
    marginTop: -18,
  },
});
