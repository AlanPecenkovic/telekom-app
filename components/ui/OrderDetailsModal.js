import { Icon } from "@rneui/base";
import React from "react";
import { Modal, StyleSheet, Text, View, Pressable } from "react-native";

const OrderDetailsModal = ({ modalVisible, dismissModal, touchedUser }) => {
  function handlePress() {
    // Do something when user card is clicked
  }
  const date = new Date().toISOString().slice(0, 10);

  return (
    <Modal
      animationType="slide"
      visible={modalVisible}
      transparent={false}
      statusBarTranslucent={true}
    >
      <View style={styles.container}>
        <View style={styles.dateContainer}>
          <View style={styles.innnerDateContainer}>
            <Text style={styles.dateText}>{date}</Text>
            <Icon name="x" type="feather" color="#7D788E" onPress={dismissModal} />
          </View>
        </View>
        <View style={styles.userContainer}>
          <Pressable onPress={handlePress}>
            <View style={styles.userInfo}>
              <Text style={styles.userId}>{touchedUser.id}</Text>
              <Text style={styles.userName}>{touchedUser.name}</Text>
            </View>
            <View style={styles.userInfo}>
              <Text style={styles.userAdress}>{touchedUser.address.street}</Text>
              <Text style={styles.userAdress}>{touchedUser.address.city}</Text>
              <Text style={styles.userAdress}>{touchedUser.address.zipcode}</Text>
            </View>
          </Pressable>
        </View>
      </View>
    </Modal>
  );
};

export default OrderDetailsModal;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "flex-start",
    alignItems: "center",
    backgroundColor: "#fff",
  },
  dateContainer: {
    justifyContent: "flex-end",
    width: "100%",
    height: 99,
    backgroundColor: "#F6F6F6",
  },
  innnerDateContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingHorizontal: 25,
    marginBottom: 19,
  },
  dateText: {
    color: "#6A657E",
    fontFamily: "montserrat400",
    fontSize: 16,
  },
  userContainer: {
    flexWrap: "wrap",
    backgroundColor: "#fff",
    width: 350,
    height: 112,
    borderRadius: 10,
    padding: 25,
    marginTop: 50,
    elevation: 6,
  },
  userInfo: {
    flexDirection: "row",
    marginBottom: 24,
    flexWrap: "wrap",
  },
  userId: {
    color: "#013A89",
    fontFamily: "montserrat400",
    fontSize: 16,
  },
  userName: {
    color: "#6A657E",
    fontFamily: "montserrat400",
    fontSize: 16,
    marginLeft: 32,
  },
  userAdress: {
    color: "#6A657E",
    fontFamily: "montserrat400",
    fontSize: 16,
    marginRight: 10,
  },
});
