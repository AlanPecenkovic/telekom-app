import { Badge, Icon } from "@rneui/base";
import React from "react";
import { StyleSheet, View } from "react-native";

const Bell_User_icons = ({ onUserPress, onBellPress }) => {
  return (
    <View style={styles.iconContainer}>
      <Icon
        name="bell"
        type="simple-line-icon"
        size={26}
        color="#6A657E"
        onPress={onBellPress}
        containerStyle={styles.icon}
      />
      <Badge
        status="error"
        value={"!"}
        containerStyle={{ position: "absolute", top: 5, left: 11 }}
        badgeStyle={{ width: 20, height: 20, borderRadius: 20 }}
        textStyle={{ fontSize: 14, fontWeight: "bold" }}
      />
      <Icon
        raised
        name="user"
        type="feather"
        size={18}
        color="#6A657E"
        containerStyle={{ elevation: 6, overflow: "hidden" }}
        onPress={onUserPress}
      />
      <Badge
        status="success"
        containerStyle={{ position: "absolute", top: 38, left: 74 }}
        badgeStyle={{ width: 9, height: 9 }}
      />
    </View>
  );
};

export default Bell_User_icons;

const styles = StyleSheet.create({
  iconContainer: {
    flexDirection: "row",
    alignItems: "center",
    marginHorizontal: 15,
  },
  icon: {
    borderRadius: 8,
    paddingRight: 10,
    overflow: "hidden",
  },
});
