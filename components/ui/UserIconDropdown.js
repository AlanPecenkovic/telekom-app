import { Icon } from "@rneui/base";
import React from "react";
import { Pressable, StyleSheet, Text, View } from "react-native";

const UserIconDropdown = ({ onLogout, onProfile, style }) => {
  return (
    <View style={[styles.container, style]}>
      <Pressable onPress={onProfile}>
        <View style={styles.innerContainer}>
          <Icon
            name="user"
            type="feather"
            size={14}
            color="#6A657E"
            iconStyle={{ marginLeft: 6 }}
          />
          <Text style={styles.text}>Profil</Text>
        </View>
      </Pressable>
      <Pressable onPress={onLogout}>
        <View style={styles.innerContainer}>
          <Icon
            name="logout"
            type="material"
            size={16}
            color="#6A657E"
            iconStyle={{ marginRight: -2, marginLeft: 8 }}
          />
          <Text style={styles.text}>Odjava</Text>
        </View>
      </Pressable>
    </View>
  );
};

export default UserIconDropdown;

const styles = StyleSheet.create({
  container: {
    width: 168,
    height: 114,
    borderRadius: 10,
    backgroundColor: "white",
    padding: 15,
    position: "absolute",
    top: 60,
    left: 130,
    elevation: 6,
  },
  innerContainer: {
    flexDirection: "row",
    alignItems: "center",
  },
  text: {
    fontFamily: "montserrat500",
    fontSize: 16,
    color: "#7D788E",
    paddingVertical: 12,
    marginLeft: 12,
  },
});
