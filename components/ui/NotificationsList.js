import React, { useContext, useEffect, useState } from "react";
import { useIsFocused } from "@react-navigation/native";
import { Button, Icon } from "@rneui/base";
import { FlatList, StyleSheet, Text, View } from "react-native";
import { UserListContext } from "../../store/userListContext";
import axios from "axios";
import { apiEndpoint } from "../../data/apiEndpoint";

const NotificationsList = ({ style }) => {
  const [notifications, setNotifications] = useState("");
  const usersCtx = useContext(UserListContext);
  const isFocused = useIsFocused();

  const DUMMY_NOTIFICATIONS = [
    {
      id: 1,
      type: "user",
      description: "Potencijalni korisnik",
    },
    {
      id: 2,
      type: "user",
      description: "Novi korisnik",
    },
    {
      id: 3,
      type: "alert-triangle",
      description: "Minimalna količina",
    },
    {
      id: 4,
      type: "alert-triangle",
      description: "Nema na stanju",
    },
    {
      id: 5,
      type: "user",
      description: "Radnik 1 je izvršio nalog",
    },
  ];

  useEffect(() => {
    let isApiSubscribed = true;
    const headersConfig = {
      Authorization: `Bearer ${usersCtx.token}`,
      "Content-Type": "application/json",
    };
    axios({
      method: "post",
      url: apiEndpoint + "api/notifications",
      data: { user_id: usersCtx.user_id },
      headers: headersConfig,
    })
      .then((response) => {
        if (isApiSubscribed) {
          setNotifications(response.data.response);
        }
      })
      .catch((error) => console.log(error));
    return () => {
      isApiSubscribed = false;
    };
  }, [isFocused]);

  let notificationsList = "";

  if (notifications !== "There are no notifications.") {
    notificationsList = (
      <FlatList
        data={notifications}
        // data={DUMMY_NOTIFICATIONS}
        keyExtractor={(item) => item.id}
        renderItem={(userData) => {
          const parsedData = JSON.parse(userData.item.data);
          // console.log(parsedData.client.name);

          return (
            <View style={styles.innerContainer}>
              <Icon
                raised
                reverse
                name="user"
                // name={userData.item.type}
                type="feather"
                size={18}
                color={userData.item.type === "alert-triangle" ? "#f0a6a6" : "#F5F5F5"}
                iconStyle={{ color: "#6A657E" }}
              />
              {userData.item.type === "App\\Notifications\\ClientAdded" ? (
                <View style={styles.addedUser}>
                  <Text style={styles.text}>
                    Korisnik dodan: {parsedData.client.name}
                  </Text>
                  <Text style={styles.date}>
                    Datum: {parsedData.client.created_at.slice(0, 10)}
                  </Text>
                </View>
              ) : (
                <Text>Tip obavijesti: {userData.item.type}</Text>
              )}
            </View>
          );
        }}
      />
    );
  } else {
    notificationsList = <Text style={styles.noNotifications}>Nema obavijesti</Text>;
  }

  return (
    <View style={[styles.container, style]}>
      {/* <FlatList
        data={notifications}
        keyExtractor={(item) => item.id}
        renderItem={(userData) => {
          return (
            <View style={styles.innerContainer}>
              <Icon
                raised
                reverse
                // name={userData.item.type}
                name="user"
                type="feather"
                size={18}
                color={
                  userData.item.type === "alert-triangle"
                    ? "#f0a6a6"
                    : "#F5F5F5"
                }
                iconStyle={{ color: "#6A657E" }}
              />

              <Text style={[styles.text, { margin: 10 }]}>
                Korisnik dodan: {userData.item.name}
              </Text>
            </View>
          );
        }}
      /> */}
      {notificationsList}
    </View>
  );
};

export default NotificationsList;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: 350,
    backgroundColor: "white",
    alignSelf: "center",
    // paddingBottom: 10,
    marginTop: 48,
    borderRadius: 10,
    overflow: "hidden",
  },
  innerContainer: {
    alignItems: "center",
    flexDirection: "row",
    borderColor: "#F1F1F3",
    borderWidth: 1,
    paddingLeft: 15,
    paddingVertical: 5,
  },
  text: {
    fontFamily: "montserrat400",
    fontSize: 14,
    color: "#7D788E",
    paddingVertical: 2,
    marginLeft: 12,
  },
  noNotifications: {
    textAlign: "center",
    marginTop: 60,
    fontSize: 20,
    fontFamily: "montserrat400",
    color: "#6A657E",
  },
  addedUser: {
    flexDirection: "column",
  },
  date: {
    fontFamily: "montserrat400",
    fontSize: 14,
    color: "#7D788E",
    paddingVertical: 2,
    marginLeft: 12,
  },
});
