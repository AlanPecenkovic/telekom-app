import React, { useState, useContext } from "react";
import { StyleSheet, View } from "react-native";
import { Button, Input } from "@rneui/themed";
import { useNavigation } from "@react-navigation/native";
import { UserListContext } from "../../store/userListContext";

const ProfileOverview = () => {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [telephone, setTelephone] = useState("");
  const [company, setCompany] = useState("");
  const [nameErrorMessage, setNameErrorMessage] = useState("");
  const [emailErrorMessage, setemailErrorMessage] = useState("");
  const [telephoneErrorMessage, setTelephoneErrorMessage] = useState("");
  const [companyErrorMessage, setCompanyErrorMessage] = useState("");
  const [borderColor, setborderColor] = useState("#D8D6DE");

  const navigation = useNavigation();

  const userContext = useContext(UserListContext);

  const nameIsValid = /^[A-Z][a-zA-z ]{4,29}$/.test(name);

  const emailIsValid = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email);

  const telephoneIsValid = /[0-9]{3}-[0-9]{3}-[0-9]{3}/.test(telephone);

  const companyIsValid = /^[a-zA-z ]{3,34}$/.test(company);

  const handleLogin = () => {
    if (nameIsValid) {
      setNameErrorMessage("");
      setborderColor("#D8D6DE");
    } else {
      setNameErrorMessage("Unesite ime i prezime!");
      setborderColor("red");
    }

    if (emailIsValid) {
      setemailErrorMessage("");
      setborderColor("#D8D6DE");
    } else {
      setemailErrorMessage("Unesite ispravnu email adresu!");
      setborderColor("red");
    }

    if (telephoneIsValid) {
      setTelephoneErrorMessage("");
      setborderColor("#D8D6DE");
    } else {
      setTelephoneErrorMessage("Unesite ispravan broj telefona!");
      setborderColor("red");
    }

    if (companyIsValid) {
      setCompanyErrorMessage("");
      setborderColor("#D8D6DE");
    } else {
      setCompanyErrorMessage("Unesite ime firme!");
      setborderColor("red");
    }

    if (nameIsValid && emailIsValid && telephoneIsValid && companyIsValid) {
      //Save the data, send API request...
    }
  };

  function handleName(enteredName) {
    setName(enteredName);
  }

  function handleEmail(enteredEmail) {
    setEmail(enteredEmail);
  }

  function handleTelephone(enteredPhoneNum) {
    setTelephone(enteredPhoneNum);
  }

  function handleCompany(enteredCompany) {
    setCompany(enteredCompany);
  }

  const resetLoginForm = () => {
    setName("");
    setEmail("");
    setTelephone("");
    setCompany("");
  };

  return (
    <View style={styles.outerContainer}>
      <View style={styles.formContainer}>
        <Input
          label="Ime i prezime"
          labelStyle={{
            color: "#7D788E",
            fontWeight: "400",
            fontSize: 16,
            fontFamily: "montserrat400",
            marginVertical: 9,
          }}
          placeholder="Ime i prezime"
          containerStyle={{
            marginHorizontal: 3,
          }}
          inputContainerStyle={{
            width: 325,
            height: 45,
            borderWidth: 1,
            borderColor: borderColor,
            borderRadius: 5,
            padding: 10,
          }}
          inputStyle={{
            fontSize: 16,
            fontFamily: "montserrat400",
          }}
          onChangeText={handleName}
          value={userContext.userData.user.name}
          errorMessage={nameErrorMessage}
          errorStyle={{ fontSize: 13 }}
        />

        <Input
          label="Email"
          labelStyle={{
            color: "#7D788E",
            fontWeight: "400",
            fontSize: 16,
            fontFamily: "montserrat400",
            marginBottom: 9,
          }}
          placeholder="email@primjer.ba"
          // leftIconContainerStyle={{ backgroundColor: "blue" }}
          containerStyle={{ marginHorizontal: 3 }}
          inputContainerStyle={{
            width: 325,
            height: 45,
            borderWidth: 1,
            borderColor: borderColor,
            borderRadius: 5,
            padding: 10,
          }}
          inputStyle={{
            fontSize: 16,
            fontFamily: "montserrat400",
          }}
          onChangeText={handleEmail}
          value={userContext.userData.user.email}
          errorMessage={emailErrorMessage}
          errorStyle={{ fontSize: 13 }}
          // onTouchEnd={} possible use of this in validation error messages
        />

        <Input
          label="Broj telefona"
          labelStyle={{
            color: "#7D788E",
            fontWeight: "400",
            fontSize: 16,
            fontFamily: "montserrat400",
            marginBottom: 9,
          }}
          placeholder="061 / 000 - 000"
          containerStyle={{ marginHorizontal: 3 }}
          inputContainerStyle={{
            width: 325,
            height: 45,
            borderWidth: 1,
            borderColor: borderColor,
            borderRadius: 5,
            padding: 10,
          }}
          inputStyle={{
            fontSize: 16,
            fontFamily: "montserrat400",
          }}
          onChangeText={handleTelephone}
          value={userContext.userData.user.phone}
          errorMessage={telephoneErrorMessage}
          errorStyle={{ fontSize: 13 }}
        />

        <Input
          label="Firma"
          labelStyle={{
            color: "#7D788E",
            fontWeight: "400",
            fontSize: 16,
            fontFamily: "montserrat400",
            marginBottom: 9,
          }}
          placeholder="Ime firme"
          containerStyle={{ marginHorizontal: 3 }}
          inputContainerStyle={{
            width: 325,
            height: 45,
            borderWidth: 1,
            borderColor: borderColor,
            borderRadius: 5,
            padding: 10,
          }}
          inputStyle={{
            fontSize: 16,
            fontFamily: "montserrat400",
          }}
          onChangeText={handleCompany}
          value={company}
          errorMessage={companyErrorMessage}
          errorStyle={{ fontSize: 13 }}
        />

        <View style={styles.buttonsContainer}>
          <View style={styles.buttonContainer}>
            <Button
              title="Spremi"
              onPress={handleLogin}
              buttonStyle={{ width: 100, height: 45 }}
              titleStyle={{
                fontFamily: "montserrat600",
                fontSize: 16,
              }}
              color="#013A89"
              radius={5}
            />
          </View>
          <View style={styles.buttonContainer}>
            <Button
              title="Poništi"
              onPress={resetLoginForm}
              buttonStyle={{
                width: 100,
                height: 45,
                borderWidth: 1,
                borderColor: "#D8D6DE",
              }}
              titleStyle={{
                color: "#7D788E",
                fontFamily: "montserrat600",
              }}
              color="#D8D6DE"
              radius={5}
              type="outline"
            />
          </View>
        </View>
      </View>
    </View>
  );
};

export default ProfileOverview;

const styles = StyleSheet.create({
  outerContainer: {
    // flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 48,
  },
  formContainer: {
    backgroundColor: "#fff",
    borderRadius: 8,
    width: 350,
    elevation: 6,
  },
  buttonsContainer: {
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
    marginVertical: 10,
    paddingHorizontal: 12,
  },
  buttonContainer: {
    borderRadius: 18,
    marginRight: 14,
  },
});
