import React from "react";
import { StyleSheet, Text, View } from "react-native";

const NumbersDropdown = ({ containerStyle }) => {
  return (
    <View style={[styles.numberDropdown, containerStyle]}>
      <Text style={styles.text}>15</Text>
      <Text style={styles.text}>25</Text>
      <Text style={styles.text}>50</Text>
    </View>
  );
};

export default NumbersDropdown;

const styles = StyleSheet.create({
  numberDropdown: {
    justifyContent: "center",
    alignItems: "center",
    width: 80,
    borderWidth: 1,
    borderColor: "#D8D6DE",
    borderRadius: 5,
    marginLeft: 105,
    marginTop: -16,
    backgroundColor: "#fff",
  },
  text: {
    fontFamily: "montserrat400",
    color: "#7D788E",
    fontSize: 14,
    paddingVertical: 8,
    alignSelf: "flex-start",
    paddingLeft: 12,
  },
});
