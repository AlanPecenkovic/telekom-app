import React, { useState } from "react";
import { StyleSheet, Text, View } from "react-native";
import { Button, Input, Icon } from "@rneui/themed";
import { useNavigation } from "@react-navigation/native";

const ChangePassForm = () => {
  const navigation = useNavigation();
  const [newPassword, setNewPassword] = useState("");
  const [confirmNewPassword, setConfirmNewPassword] = useState("");
  const [passwordErrorMessage, setPasswordErrorMessage] = useState("");
  const [confirmPasswordErrorMessage, setConfirmPasswordErrorMessage] =
    useState("");
  const [showErrorMessage, setShowErrorMessage] = useState(false);
  const [borderColor, setborderColor] = useState("#D8D6DE");

  const newPasswordIsValid = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/.test(
    newPassword
  );

  const confirmNewPasswordIsValid =
    /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/.test(confirmNewPassword);

  function handleNewPassword(enteredPassword) {
    setNewPassword(enteredPassword);
  }

  function handleConfirmNewPassword(enteredPassword) {
    setConfirmNewPassword(enteredPassword);
  }

  const submitNewPassword = () => {
    if (newPasswordIsValid) {
      console.log("New password is: ", newPassword);
      setPasswordErrorMessage("");
    } else {
      setborderColor("red");
      setPasswordErrorMessage(
        "Lozinka treba imati slova i brojeve i biti duža od 7 znakova"
      );
    }

    if (confirmNewPasswordIsValid) {
      console.log("New confirmNewPassword is: ", confirmNewPassword);
      setPasswordErrorMessage("");
    } else {
      setborderColor("red");
      setConfirmPasswordErrorMessage(
        "Lozinka treba imati slova i brojeve i biti duža od 7 znakova"
      );
    }

    if (newPassword === confirmNewPassword && newPasswordIsValid) {
      navigation.navigate("Login");
    } else {
      setShowErrorMessage(true);
    }
  };

  return (
    <View style={styles.outerContainer}>
      <View style={styles.formContainer}>
        <Text style={styles.title}>Promjena lozinke</Text>
        <Input
          label="Nova lozinka"
          labelStyle={styles.text}
          placeholder="Nova lozinka"
          leftIcon={styles.leftIcon}
          containerStyle={{ marginHorizontal: -9 }}
          inputContainerStyle={[styles.inputContainerStyle,{ borderColor: borderColor }]} // prettier-ignore
          inputStyle={{
            fontSize: 16,
            fontFamily: "montserrat400",
          }}
          secureTextEntry={true}
          onChangeText={handleNewPassword}
          value={newPassword}
          errorMessage={passwordErrorMessage}
          errorStyle={{ fontSize: 13 }}
        />
        <Input
          label="Ponovite lozinku"
          labelStyle={styles.text}
          placeholder="Ponovite lozinku"
          leftIcon={styles.leftIcon}
          // leftIconContainerStyle={{ backgroundColor: "blue" }}
          containerStyle={{ marginHorizontal: -9 }}
          inputContainerStyle={[styles.inputContainerStyle,{ borderColor: borderColor }]} // prettier-ignore
          inputStyle={{
            // backgroundColor: "#C9C9D1",
            fontSize: 16,
            fontFamily: "montserrat400",
          }}
          secureTextEntry={true}
          onChangeText={handleConfirmNewPassword}
          value={confirmNewPassword}
          errorMessage={confirmPasswordErrorMessage}
          errorStyle={{ fontSize: 13 }}
        />
        {showErrorMessage && (
          <Text style={styles.errorMessage}>
            Molimo Vas da unesete jednake lozinke !!!
          </Text>
        )}

        <View style={styles.buttonsContainer}>
          <View style={styles.buttonContainer}>
            <Button
              title="Potvrdi"
              onPress={submitNewPassword}
              buttonStyle={{ width: 100, height: 45 }}
              titleStyle={{
                fontFamily: "montserrat600",
                fontSize: 16,
              }}
              color="#013A89"
              radius={5}
            />
          </View>
        </View>
      </View>
    </View>
  );
};

export default ChangePassForm;

const styles = StyleSheet.create({
  outerContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#fff",
  },
  formContainer: {
    justifyContent: "center",
    height: 389,
    backgroundColor: "#fff",
    margin: 20,
    padding: 18,
    borderRadius: 8,
    width: "86%",
    elevation: 6,
  },
  title: {
    color: "#6A657E",
    fontFamily: "montserrat500",
    fontSize: 20,
    fontWeight: "500",
    marginBottom: 16,
  },
  inputContainerStyle: {
    width: 300,
    height: 45,
    borderWidth: 1,
    // borderColor: "#D8D6DE",
    borderRadius: 4,
    padding: 10,
  },
  leftIcon: {
    type: "octicon",
    name: "lock",
    size: 22,
    color: "#6E6B7B",
    paddingRight: 10,
  },
  text: {
    color: "#7D788E",
    fontWeight: "400",
    fontSize: 16,
    fontFamily: "montserrat400",
    marginVertical: 8,
  },
  buttonsContainer: {
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
    marginVertical: 10,
  },
  buttonContainer: {
    borderRadius: 18,
    marginRight: 14,
  },
  errorMessage: {
    color: "red",
    fontSize: 14,
    fontWeight: "bold",
  },
});
