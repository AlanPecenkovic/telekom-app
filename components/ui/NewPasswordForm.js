import React, { useState } from "react";
import { StyleSheet, Text, View } from "react-native";
import { Button, Input, Icon } from "@rneui/themed";
import { useNavigation } from "@react-navigation/native";

const NewPasswordForm = () => {
  const navigation = useNavigation();
  const [oldPassword, setOldPassword] = useState("");
  const [newPassword, setNewPassword] = useState("");
  const [confirmNewPassword, setConfirmNewPassword] = useState("");
  const [oldPasswordErrorMessage, setOldPasswordErrorMessage] = useState("");
  const [newPasswordErrorMessage, setNewPasswordErrorMessage] = useState("");
  const [confirmPasswordErrorMessage, setConfirmPasswordErrorMessage] =
    useState("");
  const [showErrorMessage, setShowErrorMessage] = useState(false);
  const [oldPasswordBorderColor, setOldPasswordBorderColor] =
    useState("#D8D6DE");
  const [newPasswordBorderColor, setNewPasswordBorderColor] =
    useState("#D8D6DE");
  const [confirmNewPasswordBorderColor, setConfirmNewPasswordBorderColor] =
    useState("#D8D6DE");

  const oldPasswordIsValid = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/.test(
    oldPassword
  );

  const newPasswordIsValid = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/.test(
    newPassword
  );

  const confirmNewPasswordIsValid =
    /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/.test(confirmNewPassword);

  function handleOldPassword(enteredPassword) {
    setOldPassword(enteredPassword);
  }

  function handleNewPassword(enteredPassword) {
    setNewPassword(enteredPassword);
  }

  function handleConfirmNewPassword(enteredPassword) {
    setConfirmNewPassword(enteredPassword);
  }

  const submitNewPassword = () => {
    if (oldPasswordIsValid) {
      console.log("Old password is: ", oldPassword);
      setOldPasswordBorderColor("#D8D6DE");
      setOldPasswordErrorMessage("");
    } else if (oldPassword.length === 0) {
      setOldPasswordBorderColor("red");
      setOldPasswordErrorMessage("Unesite staru lozinku!");
    } else if (oldPassword.length < 8) {
      setOldPasswordBorderColor("red");
      setOldPasswordErrorMessage("Lozinka mora biti duža od sedam znakova!");
    } else {
      setOldPasswordBorderColor("red");
      setOldPasswordErrorMessage("Lozinka mora imati slova i brojeve!");
    }

    if (newPasswordIsValid) {
      console.log("New password is: ", newPassword);
      setNewPasswordBorderColor("#D8D6DE");
      setNewPasswordErrorMessage("");
    } else if (newPassword.length === 0) {
      setNewPasswordBorderColor("red");
      setNewPasswordErrorMessage("Unesite novu lozinku!");
    } else if (newPassword.length < 8) {
      setNewPasswordBorderColor("red");
      setNewPasswordErrorMessage("Lozinka mora biti duža od sedam znakova!");
    } else {
      setNewPasswordBorderColor("red");
      setNewPasswordErrorMessage("Lozinka mora imati slova i brojeve!");
    }

    if (confirmNewPasswordIsValid) {
      console.log("New confirmNewPassword is: ", confirmNewPassword);
      setConfirmNewPasswordBorderColor("#D8D6DE");
      setConfirmPasswordErrorMessage("");
    } else if (confirmNewPassword.length === 0) {
      setConfirmNewPasswordBorderColor("red");
      setConfirmPasswordErrorMessage("Unesite novu lozinku!");
    } else if (confirmNewPassword.length < 8) {
      setConfirmNewPasswordBorderColor("red");
      setConfirmPasswordErrorMessage(
        "Lozinka mora biti duža od sedam znakova!"
      );
    } else {
      setConfirmNewPasswordBorderColor("red");
      setConfirmPasswordErrorMessage("Lozinka mora imati slova i brojeve!");
    }

    if (
      newPassword === confirmNewPassword &&
      newPasswordIsValid &&
      oldPassword
    ) {
      navigation.navigate("Login");
    } else {
      setShowErrorMessage(true);
    }
  };

  function handleReset() {
    setOldPassword("");
    setNewPassword("");
    setConfirmNewPassword("");
    setShowErrorMessage(false);
    setOldPasswordErrorMessage(false);
    setNewPasswordErrorMessage(false);
    setConfirmPasswordErrorMessage(false);
    setOldPasswordBorderColor("#D8D6DE");
    setNewPasswordBorderColor("#D8D6DE");
    setConfirmNewPasswordBorderColor("#D8D6DE");
  }

  return (
    <View style={styles.outerContainer}>
      <View style={styles.formContainer}>
        {/* add functionality for input field  */}
        <Input
          label="Stara lozinka"
          labelStyle={styles.text}
          placeholder="Stara lozinka"
          rightIcon={styles.rightIcon}
          containerStyle={{ marginHorizontal: -9 }}
          inputContainerStyle={[styles.inputContainerStyle,{ borderColor: oldPasswordBorderColor }]} // prettier-ignore
          inputStyle={{
            fontSize: 16,
            fontFamily: "montserrat400",
          }}
          secureTextEntry={true}
          onChangeText={handleOldPassword}
          value={oldPassword}
          errorMessage={oldPasswordErrorMessage}
          errorStyle={{ fontSize: 13 }}
        />
        <Input
          label="Nova lozinka"
          labelStyle={styles.text}
          placeholder="Nova lozinka"
          rightIcon={styles.rightIcon}
          containerStyle={{ marginHorizontal: -9 }}
          inputContainerStyle={[styles.inputContainerStyle,{ borderColor: newPasswordBorderColor }]} // prettier-ignore
          inputStyle={{
            fontSize: 16,
            fontFamily: "montserrat400",
          }}
          secureTextEntry={true}
          onChangeText={handleNewPassword}
          value={newPassword}
          errorMessage={newPasswordErrorMessage}
          errorStyle={{ fontSize: 13 }}
        />
        <Input
          label="Ponovite lozinku"
          labelStyle={styles.text}
          placeholder="Ponovite lozinku"
          rightIcon={styles.rightIcon}
          // rightIconContainerStyle={{ backgroundColor: "blue" }}
          containerStyle={{ marginHorizontal: -9 }}
          inputContainerStyle={[styles.inputContainerStyle,{ borderColor: confirmNewPasswordBorderColor }]} // prettier-ignore
          inputStyle={{
            // backgroundColor: "#C9C9D1",
            fontSize: 16,
            fontFamily: "montserrat400",
          }}
          secureTextEntry={true}
          onChangeText={handleConfirmNewPassword}
          value={confirmNewPassword}
          errorMessage={confirmPasswordErrorMessage}
          errorStyle={{ fontSize: 13 }}
        />
        {showErrorMessage && (
          <Text style={styles.errorMessage}>
            Molimo Vas unesite jednake nove lozinke !!!
          </Text>
        )}

        <View style={styles.buttonsContainer}>
          <View style={styles.buttonContainer}>
            <Button
              title="Spremi"
              onPress={submitNewPassword}
              buttonStyle={{ width: 100, height: 45 }}
              titleStyle={{
                fontFamily: "montserrat600",
                fontSize: 16,
              }}
              color="#013A89"
              radius={5}
            />
          </View>
          <Button
            title="Poništi"
            onPress={handleReset}
            buttonStyle={{
              width: 100,
              height: 45,
              borderWidth: 1,
              borderColor: "#D8D6DE",
            }}
            titleStyle={{
              fontFamily: "montserrat500",
              fontSize: 16,
              color: "#7D788E",
            }}
            color="white"
            radius={5}
          />
        </View>
      </View>
    </View>
  );
};

export default NewPasswordForm;

const styles = StyleSheet.create({
  outerContainer: {
    flex: 1,
    alignItems: "center",
    marginTop: 50,
    // marginBottom: 70,
  },
  formContainer: {
    width: 350,
    height: 400,
    backgroundColor: "#fff",
    paddingTop: 11,
    paddingHorizontal: 11,
    borderRadius: 8,
    elevation: 6,
  },
  inputContainerStyle: {
    width: 328,
    height: 45,
    borderWidth: 1,
    borderColor: "#D8D6DE",
    borderRadius: 5,
    padding: 10,
  },
  rightIcon: {
    type: "ionicon",
    name: "eye-outline",
    size: 20,
    color: "#6E6B7B",
  },
  text: {
    color: "#7D788E",
    fontWeight: "400",
    fontSize: 16,
    fontFamily: "montserrat400",
    marginBottom: 11,
  },
  buttonsContainer: {
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
    // marginBottom: 40,
  },
  buttonContainer: {
    marginRight: 14,
  },
  errorMessage: {
    color: "red",
    fontSize: 14,
    fontWeight: "bold",
  },
});
