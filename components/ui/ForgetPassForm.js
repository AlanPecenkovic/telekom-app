import React, { useState } from "react";
import { StyleSheet, Text, View } from "react-native";
import { Button, Input, Icon } from "@rneui/themed";
import { useNavigation } from "@react-navigation/native";

const ForgetPassForm = () => {
  const navigation = useNavigation();
  const [email, setEmail] = useState("");
  const [emailErrorMessage, setemailErrorMessage] = useState("");
  const [borderColor, setborderColor] = useState("#D8D6DE");

  const emailIsValid = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(
    email
  );

  function handleEmail(enteredEmail) {
    setEmail(enteredEmail);
  }

  function submitEmail() {
    if (emailIsValid) {
      console.log(
        "Send new password to email and go to change password screen."
      );
      setborderColor("#D8D6DE");
      setemailErrorMessage("");
      navigation.navigate("ChangePassword");
    } else {
      setborderColor("red");
      setemailErrorMessage("Unesite ispravnu email adresu!");
    }
  }

  return (
    <View style={styles.outerContainer}>
      <View style={styles.formContainer}>
        <Text style={styles.title}>Zaboravili ste lozinku?</Text>
        <Text style={styles.text}>
          Ne brinite. Unesite email adresu povezanu sa Vašim računom.
        </Text>
        <Input
          label="Email"
          labelStyle={styles.labelStyle}
          placeholder="Email"
          leftIcon={styles.leftIcon}
          // leftIconContainerStyle={{ backgroundColor: "blue" }}
          containerStyle={{ marginHorizontal: -9 }}
          inputContainerStyle={{
            width: 300,
            height: 45,
            borderWidth: 1,
            borderColor: borderColor,
            borderRadius: 4,
            padding: 10,
          }}
          inputStyle={{
            // backgroundColor: "#C9C9D1",
            fontSize: 16,
            fontFamily: "montserrat400",
          }}
          onChangeText={handleEmail}
          value={email}
          errorMessage={emailErrorMessage}
          errorStyle={{ fontSize: 13 }}
        />

        <View style={styles.buttonsContainer}>
          <View style={styles.buttonContainer}>
            <Button
              title="Potvrdi"
              onPress={submitEmail}
              buttonStyle={{ width: 100, height: 45 }}
              titleStyle={{
                fontFamily: "montserrat600",
                fontSize: 16,
              }}
              color="#013A89"
              radius={5}
            />
          </View>
        </View>
      </View>
    </View>
  );
};

export default ForgetPassForm;

const styles = StyleSheet.create({
  outerContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#fff",
  },
  formContainer: {
    justifyContent: "center",
    height: 389,
    backgroundColor: "#fff",
    margin: 20,
    padding: 18,
    borderRadius: 8,
    width: "86%",
    elevation: 6,
  },
  title: {
    color: "#6A657E",
    fontFamily: "montserrat500",
    fontSize: 20,
    fontWeight: "500",
    marginBottom: 16,
  },
  text: {
    color: "#7D788E",
    fontFamily: "montserrat400",
    fontSize: 16,
    marginVertical: 10,
  },
  labelStyle: {
    color: "#7D788E",
    fontWeight: "400",
    fontSize: 16,
    fontFamily: "montserrat400",
    marginVertical: 8,
  },
  leftIcon: {
    type: "octicon",
    name: "mail",
    size: 22,
    color: "#6E6B7B",
    paddingRight: 10,
  },
  buttonsContainer: {
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
    marginVertical: 10,
  },
  buttonContainer: {
    borderRadius: 18,
    marginRight: 14,
  },
});
