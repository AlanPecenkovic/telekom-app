import React from "react";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import EmployeeAdminScreen from "./components/screens/EmployeeAdminScreen";
import { Icon } from "@rneui/base";
import AddUserScreen from "./components/screens/AddUserScreen";
import StatisticScreen from "./components/screens/StatisticScreen";
import OrdersScreen from "./components/screens/OrdersScreen";
import ToolLendingScreen from "./components/screens/ToolLending";
import EmployeeAdminDetailsScreen from "./components/screens/EmployeeAdminDetailsScreen";
import ProfileScreen from "./components/screens/ProfileScreen";
import NotificationsList from "./components/ui/NotificationsList";
import LocationScreen from "./components/screens/LocationScreen";

const BottomTabsNavigation = () => {
  const BottomTabs = createBottomTabNavigator();

  return (
    <BottomTabs.Navigator
      initialRouteName="EmployeeAdmin"
      backBehavior="history"
      screenOptions={{
        tabBarStyle: { height: 130 },
        headerStyle: {
          height: 99,
          backgroundColor: "white",
          elevation: 10,
          // borderBottomWidth: 1,
        },
      }}
    >
      <BottomTabs.Screen
        name="AddUser"
        component={AddUserScreen}
        options={{
          title: "Dodavanje korisnika",
          headerTitleStyle: {
            fontFamily: "montserrat500",
            fontSize: 14,
            marginLeft: 3,
            color: "#6A657E",
          },
          tabBarShowLabel: false,
          tabBarIcon: ({ focused, color, size }) => {
            if (focused) {
              return (
                <Icon
                  reverse
                  name="md-person-add-outline"
                  type="ionicon"
                  color="#013A89"
                  size={size + 6}
                />
              );
            } else {
              return (
                <Icon
                  name="md-person-add-outline"
                  type="ionicon"
                  color="#7D788E"
                  size={size}
                />
              );
            }
          },
        }}
      />
      <BottomTabs.Screen
        name="Statistics"
        component={StatisticScreen}
        options={{
          title: "Statistika",
          headerTitleStyle: {
            fontFamily: "montserrat500",
            fontSize: 14,
            marginLeft: 3,
            color: "#6A657E",
          },
          tabBarShowLabel: false,
          tabBarIcon: ({ focused, color, size }) => {
            if (focused) {
              return (
                <Icon
                  reverse
                  name="cellular-outline"
                  type="ionicon"
                  color="#013A89"
                  size={size + 6}
                />
              );
            } else {
              return (
                <Icon
                  name="cellular-outline"
                  type="ionicon"
                  color="#7D788E"
                  size={size}
                />
              );
            }
          },
        }}
      />
      <BottomTabs.Screen
        name="EmployeeAdmin"
        component={EmployeeAdminScreen}
        options={{
          title: "Početna",
          headerTitleStyle: {
            fontFamily: "montserrat500",
            fontSize: 14,
            marginTop: 16,
            marginLeft: 3,
            color: "#6A657E",
          },
          tabBarShowLabel: false,
          tabBarIcon: ({ focused, color, size }) => {
            if (focused) {
              return (
                <Icon
                  reverse
                  name="home"
                  type="simple-line-icon"
                  color="#013A89"
                  size={size + 6}
                />
              );
            } else {
              return (
                <Icon
                  name="home"
                  type="simple-line-icon"
                  color="#7D788E"
                  size={size}
                />
              );
            }
          },
        }}
      />
      <BottomTabs.Screen
        name="EmployeeAdminDetails"
        component={EmployeeAdminDetailsScreen}
        options={{
          headerTitle: "Početna",
          headerTitleStyle: {
            fontFamily: "montserrat500",
            color: "#6A657E",
            fontSize: 14,
            marginLeft: 14,
          },
          tabBarButton: () => null,
          tabBarVisible: false,
          tabBarShowLabel: false,
          //These options hide the tab bar icon of this screen
        }}
      />
      <BottomTabs.Screen
        name="Location"
        component={LocationScreen}
        options={{
          headerTitle: "Lokacija",
          headerTitleStyle: {
            fontFamily: "montserrat500",
            color: "#6A657E",
            fontSize: 14,
            marginLeft: 14,
          },
          tabBarButton: () => null,
          tabBarVisible: false,
          tabBarShowLabel: false,
          //These options hide the tab bar icon of this screen
        }}
      />
      <BottomTabs.Screen
        name="Profile"
        component={ProfileScreen}
        options={{
          tabBarButton: () => null,
          tabBarVisible: false,
          tabBarShowLabel: false,
          //These options hide the tab bar icon of this screen
        }}
      />
      {/* <BottomTabs.Screen
        name="Notifications"
        component={NotificationsList}
        options={{
          tabBarButton: () => null,
          tabBarVisible: false,
          tabBarShowLabel: false,
          //These options hide the tab bar icon of this screen
        }}
      /> */}
      <BottomTabs.Screen
        name="ToolLending"
        component={ToolLendingScreen}
        options={{
          title: "Zaduženja",
          headerTitleStyle: {
            fontFamily: "montserrat500",
            fontSize: 14,
            marginLeft: 3,
            color: "#6A657E",
          },
          tabBarShowLabel: false,
          tabBarIcon: ({ focused, color, size }) => {
            if (focused) {
              return (
                <Icon
                  reverse
                  name="checklist"
                  type="octicon"
                  color="#013A89"
                  size={size + 6}
                />
              );
            } else {
              return (
                <Icon
                  name="checklist"
                  type="octicon"
                  color="#7D788E"
                  size={size}
                />
              );
            }
          },
        }}
      />
      <BottomTabs.Screen
        name="Orders"
        component={OrdersScreen}
        options={{
          title: "Nalozi",
          headerTitleStyle: {
            fontFamily: "montserrat500",
            fontSize: 14,
            marginLeft: 3,
            color: "#6A657E",
          },

          tabBarShowLabel: false,
          tabBarIcon: ({ focused, color, size }) => {
            if (focused) {
              return (
                <Icon
                  reverse
                  name="file-document-edit-outline"
                  type="material-community"
                  color="#013A89"
                  size={size + 6}
                />
              );
            } else {
              return (
                <Icon
                  name="file-document-edit-outline"
                  type="material-community"
                  color="#7D788E"
                  size={size}
                />
              );
            }
          },
        }}
      />
    </BottomTabs.Navigator>
  );
};

export default BottomTabsNavigation;
