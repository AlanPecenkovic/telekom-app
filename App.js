import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { StatusBar } from "expo-status-bar";
import AppLoading from "expo-app-loading";
import { useFonts } from "expo-font";
import * as SplashScreen from "expo-splash-screen";
import * as Font from "expo-font";
import LoginScreen from "./components/screens/LoginScreen";
import StartingScreen from "./components/screens/StartingScreen";
import ForgetPasswordScreen from "./components/screens/ForgetPasswordScreen";
import ChangePasswordScreen from "./components/screens/ChangePasswordScreen";
import BottomTabsNavigation from "./BottomTabsNavigation";
import AddUserDetailsScreen from "./components/screens/AddUserDetailsScreen";
import UserListContextProvider from "./store/userListContext";
import { React, useCallback, useEffect, useState } from "react";
import { View } from "react-native";
import { LogBox } from "react-native";

const Stack = createNativeStackNavigator();

export default function App() {
  LogBox.ignoreLogs([
    "expo-app-loading is deprecated in favor of expo-splash-screen:",
  ]); // Ignore log notification by message
  const [fontsLoaded] = useFonts({
    "gugi": require("./assets/fonts/Gugi-Regular.ttf"),
    "montserrat400": require("./assets/fonts/Montserrat-400.ttf"),
    "montserrat500": require("./assets/fonts/Montserrat-500.ttf"),
    "montserrat600": require("./assets/fonts/Montserrat-600.ttf"),
    "montserrat-bold": require("./assets/fonts/Montserrat-Bold.ttf"),
  });

  // if (!fontsLoaded) {
  //   return <AppLoading />;
  // }

  // if (!fontsLoaded) {
  //   SplashScreen.preventAutoHideAsync();
  // } else {
  //   SplashScreen.hideAsync();
  // }

  useEffect(() => {
    async function prepare() {
      await SplashScreen.preventAutoHideAsync();
    }
    prepare();
  }, []);

  if(!fontsLoaded){
    return null
  } else {
    SplashScreen.hideAsync()
  }

  // const onLayoutRootView = useCallback(async () => {
  //   if (fontsLoaded) {
  //     await SplashScreen.hideAsync();
  //   }
  // }, [fontsLoaded]);

  // if (!fontsLoaded) {
  //   return null;
  // }

  return (
    // <View onLayout={onLayoutRootView}>
    <>
      <StatusBar style="light" />
      <NavigationContainer>
        <UserListContextProvider>
          <Stack.Navigator
            screenOptions={{
              headerStyle: { height: 99 }, //does not want to apply
            }}
          >
            <Stack.Screen
              name="Start"
              component={StartingScreen}
              options={{
                headerShown: false,
              }}
            />
            <Stack.Screen
              name="Login"
              component={LoginScreen}
              options={{
                headerTitle: "",
                headerTransparent: true,
                headerBlurEffect: true,
              }}
            />
            <Stack.Screen
              name="ForgetPassword"
              component={ForgetPasswordScreen}
              options={{
                headerTitle: "",
                headerTransparent: true,
                headerBlurEffect: true,
              }}
            />
            <Stack.Screen
              name="ChangePassword"
              component={ChangePasswordScreen}
              options={{
                headerTitle: "",
                headerTransparent: true,
                headerBlurEffect: true,
              }}
            />
            <Stack.Screen
              name="AddUserDetailsScreen"
              component={AddUserDetailsScreen}
              options={{
                headerStyle: { height: 99 }, //does not apply
                headerTitle: "",
                // headerTransparent: true,
                // headerBlurEffect: true,
              }}
            />
            <Stack.Screen
              name="BottomTabs"
              component={BottomTabsNavigation}
              options={{
                headerShown: false,
              }}
            />
          </Stack.Navigator>
        </UserListContextProvider>
      </NavigationContainer>
      {/* </View> */}
    </>
  );
}
