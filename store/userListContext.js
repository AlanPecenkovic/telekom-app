import { useState } from "react";
import { createContext } from "react";

export const UserListContext = createContext({
  usersList: [],
  selectedUser: (id) => {},
});

function UserListContextProvider({ children }) {
  const [token, setToken] = useState("31|KdaY7UkW8pkU2FKeTfDP5zfKieOeDmJD292sQnVJ");
  const [userData, setUserData] = useState(null);
  const [user_id, setUser_id] = useState(null);
  const [usersList, setUsersList] = useState([
    {
      id: "1",
      name: "Amar Imsirovic",
      username: "Amar",
      email: "amar@gmail.com",
      address: {
        street: "Turija street",
        suite: "Apt. 556",
        city: "Bihac",
        zipcode: "77000",
      },
      phone: "1-770-736-8031 x56442",
      website: "amarwebdev.net",
    },
    {
      id: "2",
      name: "Ervin Howell",
      username: "Antonette",
      email: "Shanna@melissa.tv",
      address: {
        street: "Victor Plains",
        suite: "Suite 879",
        city: "Wisokyburgh",
        zipcode: "90566",
      },
      phone: "010-692-6593 x09125",
      website: "anastasia.net",
    },
    {
      id: "3",
      name: "Clementine Bauch",
      username: "Samantha",
      email: "Nathan@yesenia.net",
      address: {
        street: "Douglas",
        suite: "Suite 847",
        city: "McKenziehaven",
        zipcode: "59590",
      },
      phone: "1-463-123-4447",
      website: "ramiro.info",
    },
    {
      id: 4,
      name: "Patricia Lebsack",
      username: "Karianne",
      email: "Julianne.OConner@kory.org",
      address: {
        street: "Hoeger Mall",
        suite: "Apt. 692",
        city: "South Elvis",
        zipcode: "53919",
      },
      phone: "493-170-9623 x156",
      website: "kale.biz",
    },
    {
      id: 5,
      name: "Chelsey Dietrich",
      username: "Kamren",
      email: "Lucio_Hettinger@annie.ca",
      address: {
        street: "Skiles Walks",
        suite: "Suite 351",
        city: "Roscoeview",
        zipcode: "33263",
      },
      phone: "(254)954-1289",
      website: "demarco.info",
    },
    {
      id: 6,
      name: "Mrs. Dennis Schultz",
      username: "Leopoldo_Corkery",
      email: "Karley_Dach@jasper.info",
      address: {
        street: "La Mesa",
        suite: "Apt. 950",
        city: "San Diego",
        zipcode: "22400",
      },
      phone: "1-477-935-8478 x6430",
      website: "ola.org",
    },
    {
      id: 7,
      name: "Kurtis Weissnat",
      username: "Elwyn.Skiles",
      email: "Telly.Hoeger@billy.biz",
      address: {
        street: "Rex Trail",
        suite: "Suite 280",
        city: "Howemouth",
        zipcode: "58804",
      },
      phone: "210.067.6132",
      website: "elvis.io",
    },
    {
      id: 8,
      name: "Nicholas Runolfsdottir",
      username: "Maxime_Nienow",
      email: "Sherwood@rosamond.me",
      address: {
        street: "Ellsworth Summit",
        suite: "Suite 729",
        city: "Aliyaview",
        zipcode: "45169",
      },
      phone: "586.493.6943 x140",
      website: "jacynthe.com",
    },
    {
      id: 9,
      name: "Glenna Reichert",
      username: "Delphine",
      email: "Chaim_McDermott@dana.io",
      address: {
        street: "Dayna Park",
        suite: "Suite 449",
        city: "Berthold",
        zipcode: "76495",
      },
      phone: "(775)976-6794 x41206",
      website: "conrad.com",
    },
    {
      id: 10,
      name: "Clementina DuBuque",
      username: "Moriah.Stanton",
      email: "Rey.Padberg@karina.biz",
      address: {
        street: "Kattie Turnpike",
        suite: "Suite 198",
        city: "Lebsackbury",
        zipcode: "31428",
      },
      phone: "024-648-3804",
      website: "ambrose.net",
    },
  ]);
  function selectedUser(id) {
    setUsersList((currentUsers) => currentUsers.filter((user) => user.id === id));
  }
  function saveToken(token) {
    setToken(token);
  }
  function saveUserData(user) {
    setUserData(user);
  }
  function saveId(id) {
    setUser_id(id);
  }
  const value = {
    usersList: usersList,
    selectedUser: selectedUser,
    saveToken: saveToken,
    saveUserData: saveUserData,
    saveId: saveId,
    token: token,
    userData: userData,
    user_id: user_id,
  };
  return <UserListContext.Provider value={value}>{children}</UserListContext.Provider>;
}

export default UserListContextProvider;
